# Simple Weight and Calorie Tracker

SWaCT is a minimal application for recording and viewing/visuallizing weight, calorie, and various measurements.

It was initially written for personal use but is shared here under a permissive license simply because there's no harm in it.

## Screenshots

![](app/src/main/res/drawable/Screenshot_20200103-224226.png)
![](app/src/main/res/drawable/Screenshot_20200103-224252.png)
![](app/src/main/res/drawable/Screenshot_20200103-224306.png)
![](app/src/main/res/drawable/Screenshot_20200103-224324.png)
![](app/src/main/res/drawable/Screenshot_20200103-224341.png)

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.