<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:orientation="vertical">

    <!-- Chart section -->
    <androidx.cardview.widget.CardView
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_weight="1"
        app:cardCornerRadius="@dimen/cardview_radius"
        app:cardElevation="@dimen/cardview_elevation"
        app:contentPadding="@dimen/cardview_padding"
        app:cardUseCompatPadding="true">

        <com.github.mikephil.charting.charts.BarChart
            android:id="@+id/calories_overview_chart"
            android:layout_width="match_parent"
            android:layout_height="match_parent" />

    </androidx.cardview.widget.CardView>

    <!-- Contains the info section (contains current daily and goal calories) and the add button -->
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal">

        <!-- Info section.  Daily calorie sum and daily goal. -->
        <androidx.cardview.widget.CardView
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            app:cardCornerRadius="@dimen/cardview_radius"
            app:cardElevation="@dimen/cardview_elevation"
            app:contentPadding="@dimen/cardview_padding"
            app:cardUseCompatPadding="true">

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="horizontal"
                android:baselineAligned="false">

                <!-- Shows the calories consumed so far today -->
                <LinearLayout
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:layout_gravity="center_vertical"
                    android:gravity="center_horizontal"
                    android:orientation="vertical">

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:textSize="18sp"
                        android:textColor="@color/color_primary"
                        android:text="@string/today_label" />

                    <RelativeLayout
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content">

                        <TextView
                            android:id="@+id/calories_overview_todays_calories"
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_alignParentLeft="true"
                            android:layout_alignParentStart="true"
                            android:layout_centerInParent="true"
                            android:textSize="24sp"
                            android:text="@string/calories_overview_todays_calories_default"/>

                        <TextView
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_toRightOf="@+id/calories_overview_todays_calories"
                            android:layout_toEndOf="@+id/calories_overview_todays_calories"
                            android:layout_marginLeft="2sp"
                            android:layout_marginStart="2sp"
                            android:layout_marginTop="2sp"
                            android:textSize="12sp"
                            android:text="@string/calories_units" />

                    </RelativeLayout>

                </LinearLayout>

                <!-- States the daily caloric goal, if one exists -->
                <LinearLayout
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:layout_gravity="center_vertical"
                    android:gravity="center_horizontal"
                    android:orientation="vertical">

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:textSize="18sp"
                        android:textColor="@color/color_primary"
                        android:text="@string/goal_label" />

                    <RelativeLayout
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content">

                        <TextView
                            android:id="@+id/calories_overview_goal_calories"
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_alignParentLeft="true"
                            android:layout_alignParentStart="true"
                            android:layout_centerInParent="true"
                            android:textSize="24sp"
                            android:text="@string/calories_overview_goal_calories_default"/>

                        <TextView
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_toRightOf="@+id/calories_overview_goal_calories"
                            android:layout_toEndOf="@+id/calories_overview_goal_calories"
                            android:layout_marginLeft="2sp"
                            android:layout_marginStart="2sp"
                            android:layout_marginTop="2sp"
                            android:textSize="12sp"
                            android:text="@string/calories_units" />

                    </RelativeLayout>

                </LinearLayout>

                <!-- Shows the calories remaining to the goal, for today -->
                <LinearLayout
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:layout_gravity="center_vertical"
                    android:gravity="center_horizontal"
                    android:orientation="vertical">

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:textSize="18sp"
                        android:textColor="@color/color_primary"
                        android:text="@string/calories_overview_to_goal_label" />

                    <RelativeLayout
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content">

                        <TextView
                            android:id="@+id/calories_overview_to_goal_calories"
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_alignParentLeft="true"
                            android:layout_alignParentStart="true"
                            android:layout_centerInParent="true"
                            android:textSize="24sp"
                            android:text="@string/calories_overview_to_goal_calories_default"/>

                        <TextView
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_toRightOf="@+id/calories_overview_to_goal_calories"
                            android:layout_toEndOf="@+id/calories_overview_to_goal_calories"
                            android:layout_marginLeft="2sp"
                            android:layout_marginStart="2sp"
                            android:layout_marginTop="2sp"
                            android:textSize="12sp"
                            android:text="@string/calories_units" />

                    </RelativeLayout>

                </LinearLayout>

            </LinearLayout>

        </androidx.cardview.widget.CardView>

        <com.google.android.material.floatingactionbutton.FloatingActionButton
            android:id="@+id/calories_overview_floating_button"
            style="@style/Widget.MaterialComponents.FloatingActionButton"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_margin="@dimen/fab_margin_base"
            android:src="@drawable/ic_add_white_24dp" />

    </LinearLayout>

</LinearLayout>