package luphi.swact;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import luphi.swact.models.Calories;
import luphi.swact.models.Measurement;
import luphi.swact.models.Weight;
import luphi.swact.views.profile.ProfileFragment;
import luphi.swact.views.SWaCTFragment;
import luphi.swact.views.calorie.CalorieDrawerFragment;
import luphi.swact.views.home.HomeFragment;
import luphi.swact.views.measurement.MeasurementDrawerFragment;
import luphi.swact.views.weight.WeightDrawerFragment;

/**
 * This app's only Activity; performs all routines related to the Android lifecycle
 */
public class SWaCTActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    /**
     * A reference to the options menu which appears in top-right of the application in the app bar
     */
    private Menu mOptionsMenu = null;

    /**
     * A reference the currently-shown fragment, defaulting to the home fragment
     */
    private String mActiveFragment = "home_drawer";

    /**
     * Formatter for the date using the localized, shorthand (e.g. 11/7/2004)
     */
    private DateFormat mDateFormat = DateFormat.getDateInstance(DateFormat.SHORT,
            Locale.getDefault());

    /**
     * Formatter for the time of day of each calorie entry, in the "hh:mm aa" format
     */
    private DateFormat mTimeFormat = new SimpleDateFormat("hh:mm aa", Locale.getDefault());

    /**
     * This flag indicates the app/activity was revived from a memory ejection event.  These events
     * occur when the app has been unused for long enough or when night mode is enabled, to name a
     * couple examples.  This flag allows for things to be cleaned up in such events.
     */
    private boolean mWasRevived = false;

    /**
     * Call by the system when the application/activity is started; sets up the fragments to be
     * exchanged by the drawer navigation view
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Visually select the home drawer (at index 0)
        navigationView.getMenu().getItem(0).setChecked(true);
        // Register onNavigationItemSelected to listen for new drawer selections
        navigationView.setNavigationItemSelectedListener(this);
        // Read the histories and profile information
        Record.read(this);
        // Insert the initial (home) fragment into the main content container
        getSupportFragmentManager().beginTransaction().replace(R.id.main_content_container,
                new HomeFragment(), "home_drawer").commit();
        // If the bundle is not null, this activity is being recreated
        if (savedInstanceState != null)
            mWasRevived = true;
    }

    /**
     * Called by the system when the activity is brought to the forward or, in other words, when the
     * view has been created and it's visible; this method is overridden here only to handle the
     * case in which the app was ejected from memory and subsequently revived
     */
    @Override
    public void onResume() {
        super.onResume();
        // If the app was previously killed
        if (mWasRevived) {
            // The app will default to the home drawer so set it as the selected one just in case
            NavigationView navigationView = findViewById(R.id.nav_view);
            // Deselect all drawers
            for (int i = 0; i < navigationView.getMenu().size(); i++)
                navigationView.getMenu().getItem(i).setChecked(false);
            // Select/highlight only the newly-selected drawer index
            navigationView.getMenu().getItem(0).setChecked(true);
            mWasRevived = false;
        }
    }

    /**
     * Called by the system when the options menu is created; stores a reference to the menu and
     * inflates the initial (home) options
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mOptionsMenu = menu;
        // Inflate the menu; this adds items to the action bar at the top of the window
        getMenuInflater().inflate(R.menu.options_menu_home, menu);
        return true;
    }

    /**
     * Called by the system when an option in the options menu (in the app bar) is selected;
     * responds in different ways depending on the ID selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        final Context context = this;
        DialogInterface.OnClickListener onClickListener;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        switch (id) {
            case R.id.clear_everything:
                onClickListener = new
                        DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case DialogInterface.BUTTON_POSITIVE:
                                        Record.Weights.clear();
                                        Record.Calories.clear();
                                        Record.Measurements.clear();
                                        clearCommon(context);
                                        break;
                                    case DialogInterface.BUTTON_NEGATIVE:
                                        // Nothing to do
                                        break;
                                }
                            }
                        };
                builder.setMessage(getResources().getString(R.string.clear_everythig_are_you_sure)).
                        setPositiveButton("Yes", onClickListener).
                        setNegativeButton("No", onClickListener).show();
                break;
            case R.id.clear_weight:
                onClickListener = new
                        DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case DialogInterface.BUTTON_POSITIVE:
                                        Record.Weights.clear();
                                        clearCommon(context);
                                        break;
                                    case DialogInterface.BUTTON_NEGATIVE:
                                        // Nothing to do
                                        break;
                                }
                            }
                        };
                builder.setMessage(getResources().getString(R.string.clear_weight_are_you_sure)).
                        setPositiveButton("Yes", onClickListener).
                        setNegativeButton("No", onClickListener).show();
                break;
            case R.id.clear_calories:
                onClickListener = new
                        DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case DialogInterface.BUTTON_POSITIVE:
                                        Record.Calories.clear();
                                        clearCommon(context);
                                        break;
                                    case DialogInterface.BUTTON_NEGATIVE:
                                        // Nothing to do
                                        break;
                                }
                            }
                        };
                builder.setMessage(getResources().getString(R.string.clear_calories_are_you_sure)).
                        setPositiveButton("Yes", onClickListener).
                        setNegativeButton("No", onClickListener).show();
                break;
            case R.id.clear_measurements:
                onClickListener = new
                        DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case DialogInterface.BUTTON_POSITIVE:
                                        Record.Measurements.clear();
                                        clearCommon(context);
                                        break;
                                    case DialogInterface.BUTTON_NEGATIVE:
                                        // Nothing to do
                                        break;
                                }
                            }
                        };
                builder.setMessage(getResources().
                        getString(R.string.clear_measurements_are_you_sure)).
                        setPositiveButton("Yes", onClickListener).
                        setNegativeButton("No", onClickListener).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Called by the system when a navigation drawer is selected; opens the appropriate drawer
     * by switching out the active fragment with the selected drawer's fragment
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        openDrawerByID(item.getItemId());
        return true;
    }

    /**
     * Called by the system on a back event; defers to the active fragment if the fragment has a use
     * for the event, then returns to the home drawer if another drawer is open, and calls on the
     * parent method failing the previous two options (likely exiting this app)
     */
    @Override
    public void onBackPressed() {
        // If the active fragment consumed (AKA made use of) the back event, stop here
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(mActiveFragment);
        if ((fragment != null) && ((SWaCTFragment) fragment).onBackPressed())
            return;
        // If the open drawer is not the home drawer, return to the home drawer
        if (!mActiveFragment.equals("home_drawer"))
            openDrawerByID(R.id.nav_home);
            // Let the system handle it (likely exit the app)
        else
            super.onBackPressed();
    }

    /**
     * Open the given drawer ID by hiding the active fragment, showing the selected fragment, and
     * setting the option menu associated with that drawer
     *
     * @param navigationID The navigation ID of the drawer to open
     */
    public void openDrawerByID(int navigationID) {
        int optionsMenuID, // The ID of the menu (in res/menu) to inflate
                drawerIndex; // The index of the drawer to highlight/select
        // Determine which fragment should be inserted into the content container by choosing the
        // appropriate fragment tag
        String fragmentTagToInsert;
        switch (navigationID) {
            case R.id.nav_home:
                fragmentTagToInsert = "home_drawer";
                optionsMenuID = R.menu.options_menu_home;
                drawerIndex = 0;
                break;
            case R.id.nav_weight:
                fragmentTagToInsert = "weight_drawer";
                optionsMenuID = R.menu.options_menu_weight;
                drawerIndex = 1;
                break;
            case R.id.nav_calories:
                fragmentTagToInsert = "calorie_drawer";
                optionsMenuID = R.menu.options_menu_calories;
                drawerIndex = 2;
                break;
            case R.id.nav_measurements:
                fragmentTagToInsert = "measurement_drawer";
                optionsMenuID = R.menu.options_menu_measurements;
                drawerIndex = 3;
                break;
            case R.id.nav_profile:
                fragmentTagToInsert = "profile_drawer";
                optionsMenuID = -1; // The profile fragment has no need for the menu
                drawerIndex = 4;
                break;
            default:
                return;
        }
        // Retrieve the fragment by tag from the fragment manager if it has previously been inserted
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(fragmentTagToInsert);
        // If it was previously created and inserted
        if (fragment != null)
            fragmentManager.beginTransaction().replace(R.id.main_content_container,
                    fragment).commit();
            // If the fragment must be created before being inserted
        else {
            switch (fragmentTagToInsert) {
                case "home_drawer":
                default:
                    fragment = new HomeFragment();
                    break;
                case "weight_drawer":
                    fragment = new WeightDrawerFragment();
                    break;
                case "calorie_drawer":
                    fragment = new CalorieDrawerFragment();
                    break;
                case "measurement_drawer":
                    fragment = new MeasurementDrawerFragment();
                    break;
                case "profile_drawer":
                    fragment = new ProfileFragment();
                    break;
            }
            fragmentManager.beginTransaction().replace(R.id.main_content_container, fragment,
                    fragmentTagToInsert).commit();
        }
        mActiveFragment = fragmentTagToInsert;
        // Remove all menu options from the toolbar
        mOptionsMenu.clear();
        // If the menu ID is valid, inflate the menu (based on the navigation ID)
        if (optionsMenuID != -1)
            getMenuInflater().inflate(optionsMenuID, mOptionsMenu);
        // Close the drawer, AKA slide it offscreen
        ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Deselect all drawers
        for (int i = 0; i < navigationView.getMenu().size(); i++)
            navigationView.getMenu().getItem(i).setChecked(false);
        // Select/highlight only the newly-selected drawer index
        navigationView.getMenu().getItem(drawerIndex).setChecked(true);
    }

    /**
     * Launch a dialog to record a new weight measurement
     */
    public void addWeight() {
        View view = View.inflate(this, R.layout.dialog_add_weight, null);
        TextView unitsText = view.findViewById(R.id.add_weight_units);
        final Button dateButton = view.findViewById(R.id.add_weight_date_button);
        final EditText input = view.findViewById(R.id.add_weight_input),
                noteInput = view.findViewById(R.id.add_weight_note);
        final Calendar calendar = Calendar.getInstance(); // Initializes to the current time
        final Context context = this;
        dateButton.setText(mDateFormat.format(calendar.getTime()));
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month,
                                                  int day) {
                                calendar.set(year, month, day);
                                // Changes the button's content to show the newly-chosen date
                                dateButton.setText(mDateFormat.format(calendar.getTime()));
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        if (Record.useImperialWeight)
            unitsText.setText(getResources().getString(R.string.imperial_weight_units));
        else
            unitsText.setText(getResources().getString(R.string.metric_weight_units));
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setView(view);
        dialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int id) {
                float weightValue;
                // Some sanitation
                try {
                    weightValue = Float.parseFloat(input.getText().toString());
                } catch (Exception e) {
                    return;
                }
                if (weightValue <= 0f)
                    return;
                // If the user opted for imperial units (while records use metric)
                if (Record.useImperialWeight)
                    weightValue = Utils.poundsToKilograms(weightValue);
                // Check for a note
                String note = noteInput.getText().toString().trim();
                // Generate the new record
                final Weight newWeight = new Weight(calendar.getTime(), weightValue,
                        !note.isEmpty() ? note : null);
                // Only one weight record is permitted per day so, if there is an existing record
                // for the given day, it must first be removed.  If this weight object is not null
                // after the search, it means a record for this day exists.
                Weight existingWeight = null;
                for (Weight recordedWeight : Record.Weights) {
                    if (Utils.isSameDay(newWeight.date, recordedWeight.date)) {
                        existingWeight = recordedWeight;
                        break;
                    }
                }
                // If an existing record was found
                if (existingWeight != null) {
                    final Weight toRemove = existingWeight; // Needed for scope reasons
                    DialogInterface.OnClickListener onClickListener = new
                            DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            // The new weight should share a key with the old
                                            // weight, thus just inserting a new value, but just in
                                            // case it will be removed from disk here
                                            Record.remove(context, toRemove.key());
                                            Record.Weights.remove(toRemove);
                                            addWeight(context, newWeight);
                                            break;
                                        case DialogInterface.BUTTON_NEGATIVE:
                                            // Nothing to do
                                            break;
                                    }
                                }
                            };
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(getResources().getString(R.string.add_weight_are_you_sure)).
                            setPositiveButton("Yes", onClickListener).
                            setNegativeButton("No", onClickListener).show();
                } else
                    addWeight(context, newWeight);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int id) {
                dialogInterface.cancel();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    /**
     * Launch a dialog to record a new calorie measurement
     */
    public void addCalories() {
        View view = View.inflate(this, R.layout.dialog_add_calories, null);
        final Button dateButton = view.findViewById(R.id.add_calories_date_button),
                timeButton = view.findViewById(R.id.add_calories_time_button);
        final EditText input = view.findViewById(R.id.add_calories_input),
                noteInput = view.findViewById(R.id.add_calories_note);
        final Calendar dateCalendar = Calendar.getInstance(), // Initializes to the current time
                timeCalendar = Calendar.getInstance();
        final Context context = this;
        dateButton.setText(mDateFormat.format(dateCalendar.getTime()));
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month,
                                                  int day) {
                                dateCalendar.set(year, month, day);
                                // Changes the button's content to show the newly-chosen date
                                dateButton.setText(mDateFormat.format(dateCalendar.getTime()));
                            }
                        }, dateCalendar.get(Calendar.YEAR), dateCalendar.get(Calendar.MONTH),
                        dateCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        timeButton.setText(mTimeFormat.format(timeCalendar.getTime()));
        timeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hour, int minute) {
                        timeCalendar.set(Calendar.HOUR, hour);
                        timeCalendar.set(Calendar.MINUTE, minute);
                        // Changes the button's content to show the newly-chosen date
                        timeButton.setText(mDateFormat.format(timeCalendar.getTime()));
                    }
                }, timeCalendar.get(Calendar.HOUR_OF_DAY),
                        timeCalendar.get(Calendar.MINUTE), false).show(); // false = 12-hour format
            }
        });
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setView(view);
        dialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int id) {
                int caloriesValue;
                // Some sanitation
                try {
                    caloriesValue = Integer.parseInt(input.getText().toString());
                } catch (Exception e) {
                    return;
                }
                if (caloriesValue <= 0)
                    return;
                // Merge the time calendar into the date calendar
                dateCalendar.set(Calendar.HOUR_OF_DAY, timeCalendar.get(Calendar.HOUR_OF_DAY));
                dateCalendar.set(Calendar.MINUTE, timeCalendar.get(Calendar.MINUTE));
                dateCalendar.set(Calendar.SECOND, timeCalendar.get(Calendar.SECOND));
                dateCalendar.set(Calendar.MILLISECOND, timeCalendar.get(Calendar.MILLISECOND));
                // Check for a note
                String note = noteInput.getText().toString().trim();
                // Generate the new record
                Calories newCalories = new Calories(dateCalendar.getTime(), caloriesValue,
                        !note.isEmpty() ? note : null);
                Record.Calories.add(newCalories);
                // Because the date/time isn't necessarily in the correct order, sort the records
                Collections.sort(Record.Calories, new Record.CaloriesDateComparator());
                // Save and update what visible fragment
                Record.write(context);
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(mActiveFragment);
                if (fragment != null)
                    ((SWaCTFragment) fragment).refresh();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int id) {
                dialogInterface.cancel();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    /**
     * Launch a dialog to record a new measurement value with an associated type
     */
    public void addMeasurement() {
        View view = View.inflate(this, R.layout.dialog_add_measurements, null);
        final Button dateButton = view.findViewById(R.id.add_measurements_date_button);
        final Spinner spinner = view.findViewById(R.id.add_measurements_spinner);
        final EditText input = view.findViewById(R.id.add_measurements_input),
                noteInput = view.findViewById(R.id.add_measurements_note);
        final Calendar calendar = Calendar.getInstance(); // Initializes to the current time
        final Context context = this;
        dateButton.setText(mDateFormat.format(calendar.getTime()));
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month,
                                                  int day) {
                                calendar.set(year, month, day);
                                // Changes the button's content to show the newly-chosen date
                                dateButton.setText(mDateFormat.format(calendar.getTime()));
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, Record.MeasurementsTypes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setView(view);
        dialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Some sanitation
                if ((spinner.getSelectedItem() == null) ||
                        spinner.getSelectedItem().toString().isEmpty())
                    return;
                float measurementValue;
                try {
                    measurementValue = Float.parseFloat(input.getText().toString());
                } catch (Exception e) {
                    return;
                }
                if ((measurementValue <= 0f) || spinner.getSelectedItem().toString().isEmpty())
                    return;
                // Check for a note
                String note = noteInput.getText().toString().trim();
                // Generate the new record
                final Measurement newMeasurement = new Measurement(calendar.getTime(),
                        spinner.getSelectedItem().toString(), measurementValue,
                        !note.isEmpty() ? note : null);
                // Only one measurement record is permitted per day per type so, if there is an
                // existing record for the given day and type, it must first be removed.  If this
                // measurement object is not null after the search, it means a record for this day
                // exists.
                Measurement existingMeasurement = null;
                for (Measurement recordedMeasurement : Record.Measurements) {
                    if (Utils.isSameDay(newMeasurement.date, recordedMeasurement.date) &&
                            newMeasurement.type.equals(recordedMeasurement.type)) {
                        existingMeasurement = recordedMeasurement;
                        break;
                    }
                }
                // If an existing record was found
                if (existingMeasurement != null) {
                    final Measurement toRemove = existingMeasurement; // Needed for scope reasons
                    DialogInterface.OnClickListener onClickListener = new
                            DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            // The new measurement should share a key with the old
                                            // measurement, thus just inserting a new value, but
                                            // just in case it will be removed from disk here
                                            Record.remove(context, toRemove.key());
                                            Record.Measurements.remove(toRemove);
                                            addMeasurement(context, newMeasurement);
                                            break;
                                        case DialogInterface.BUTTON_NEGATIVE:
                                            // Nothing to do
                                            break;
                                    }
                                }
                            };
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(getResources().
                            getString(R.string.add_measurement_confirmation)).
                            setPositiveButton("Yes", onClickListener).
                            setNegativeButton("No", onClickListener).show();
                } else
                    addMeasurement(context, newMeasurement);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    /**
     * Launch a dialog to create a new type of measurement
     */
    public void addMeasurementsType() {
        View view = View.inflate(this, R.layout.dialog_add_measurements_type, null);
        final EditText input = view.findViewById(R.id.add_measurements_type_input);
        final Context context = this;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setView(view);
        dialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int id) {
                String newType = input.getText().toString();
                if (newType.isEmpty() || Record.MeasurementsTypes.contains(newType))
                    return;
                Record.MeasurementsTypes.add(newType);
                Collections.sort(Record.MeasurementsTypes);
                // Save and update what visible fragment
                Record.write(context);
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(mActiveFragment);
                if (fragment != null)
                    ((SWaCTFragment) fragment).refresh();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int id) {
                dialogInterface.cancel();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    /**
     * Launch a dialog to select an existing type of measurement to remove along with all entries
     * using that type
     */
    public void removeMeasurementsType() {
        View view = View.inflate(this, R.layout.dialog_remove_measurement_type, null);
        final Spinner spinner = view.findViewById(R.id.remove_measurements_type_spinner);
        final Context context = this;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, Record.MeasurementsTypes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setView(view);
        dialogBuilder.setPositiveButton("Remove", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int id) {
                String typeToRemove = spinner.getSelectedItem().toString();
                if (typeToRemove.isEmpty() || !Record.MeasurementsTypes.contains(typeToRemove))
                    return;
                // Remove the type itself
                Record.MeasurementsTypes.remove(typeToRemove);
                List<Measurement> entriesToRemove = new LinkedList<>();
                for (Measurement measurement : Record.Measurements) {
                    if (measurement.type.equals(typeToRemove))
                        entriesToRemove.add(measurement);
                }
                // Remove all measurements entries associated with the type
                Record.Measurements.removeAll(entriesToRemove);
                // Save and update what visible fragment
                Record.write(context);
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(mActiveFragment);
                if (fragment != null)
                    ((SWaCTFragment) fragment).refresh();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int id) {
                dialogInterface.cancel();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    /**
     * Helper function to perform common tasks related to clearing any history
     *
     * @param context The application Context
     */
    private void clearCommon(Context context) {
        Record.clear(context);
        Record.write(context);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(mActiveFragment);
        if (fragment != null)
            ((SWaCTFragment) fragment).refresh();
    }

    /**
     * Helper function to perform the actual addition (to record) of a new weight
     *
     * @param context The application Context
     * @param weight  The Weight to be recorded
     */
    private void addWeight(Context context, Weight weight) {
        // Save the new one
        Record.write(context, weight.key(), weight.value());
        Record.Weights.add(weight);
        // Because the date isn't necessarily in the correct order, sort the records
        Collections.sort(Record.Weights, new Record.WeightDateComparator());
        // Update the active fragment
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(mActiveFragment);
        if (fragment != null)
            ((SWaCTFragment) fragment).refresh();
    }

    /**
     * Helper function to perform the actual addition (to record) of a new measurement
     *
     * @param context     The application Context
     * @param measurement The Measurement to be recorded
     */
    private void addMeasurement(Context context, Measurement measurement) {
        // Save the new one
        Record.write(context, measurement.key(), measurement.value());
        Record.Measurements.add(measurement);
        // Because the date isn't necessarily in the correct order, sort the records
        Collections.sort(Record.Measurements, new Record.MeasurementsDateComparator());
        // Update the active fragment
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(mActiveFragment);
        if (fragment != null)
            ((SWaCTFragment) fragment).refresh();
    }
}
