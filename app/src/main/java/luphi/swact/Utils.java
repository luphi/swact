package luphi.swact;

import android.text.format.DateUtils;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A collection of static helper methods
 */
public class Utils {
    /**
     * Formatter for the date using the localized, shorthand (e.g. 11/7/2004)
     */
    private static DateFormat mDateFormat = DateFormat.getDateInstance(DateFormat.SHORT,
            Locale.getDefault());

    /**
     * An equals comparison for floating point values with a margin of error built in with that
     * margin being relative to the larger value, specifically plus or minus 0.1% of said value
     *
     * @param one A float value to be compared
     * @param two A float value to be compared
     * @return True if the two values are approximately equal
     */
    public static boolean fuzzyEquals(float one, float two) {
        float difference = Math.abs(one - two);
        float maximum = Math.max(one, two);
        // True if the difference is less than 0.1% of the larger value
        return difference <  (maximum * 0.001f);
    }

    /**
     * Converts an imperial weight (pound) value to the equivalent metric (kilogram) value
     *
     * @param pounds An imperial weight measurement
     * @return The equivalent metric weight measurement
     */
    public static float poundsToKilograms(float pounds) {
        return pounds * 0.453592f;
    }

    /**
     * Converts a metric weight (kilogram) value to the equivalent imperial (pound) value
     *
     * @param kilograms A metric weight measurement
     * @return The equivalent imperial weight measurement
     */
    public static float kilogramsToPounds(float kilograms) {
        return kilograms * 2.20462f;
    }

    /**
     * Converts an imperial height (inch) value to the equivalent metric (centimeter) value
     *
     * @param inches An imperial height/length measurement
     * @return The equivalent metric height/length measurement
     */
    public static int inchesToCentimeters(int inches) {
        return Math.round((float) inches * 2.54f);
    }

    /**
     * Converts a metric height (centimeter) value to the equivalent imperial (inch) value
     *
     * @param centimeters A metric height/length measurement
     * @return The equivalent imperial height/length measurement
     */
    public static int centimetersToInches(int centimeters) {
        return Math.round((float) centimeters * 0.393701f);
    }

    /**
     * Creates a Date object from another with the resulting object being set to midnight at the
     * start of the given day
     *
     * @param date A Date object, independent of the time of day
     * @return The given date at midnight at the start of the day
     */
    public static Date startOfDay(Date date) {
        Calendar startOfDay = Calendar.getInstance();
        startOfDay.setTime(date);
        startOfDay.set(Calendar.HOUR_OF_DAY, 0);
        startOfDay.set(Calendar.MINUTE, 0);
        startOfDay.set(Calendar.SECOND, 0);
        startOfDay.set(Calendar.MILLISECOND, 0);
        return startOfDay.getTime();
    }

    /**
     * Creates a Date object from another with the resulting object being set to noon at the
     * middle of the given day
     *
     * @param date A Date object, independent of the time of day
     * @return The given date at noon at the middle of the day
     */
    public static Date middleOfDay(Date date) {
        Calendar endOfDay = Calendar.getInstance();
        endOfDay.setTime(date);
        endOfDay.set(Calendar.HOUR_OF_DAY, 12);
        endOfDay.set(Calendar.MINUTE, 0);
        endOfDay.set(Calendar.SECOND, 0);
        endOfDay.set(Calendar.MILLISECOND, 0);
        return endOfDay.getTime();
    }

    /**
     * A comparison of two Calendar objects determining if the two objects are set to the same
     * day/date, independent of the time of day
     *
     * @param calendar1 A Calendar object
     * @param calendar2 A Calendar object
     * @return True if the Calendars are set to the same day/date
     */
    public static boolean isSameDay(Calendar calendar1, Calendar calendar2) {
        return (calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR)) &&
                (calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR));
    }

    /**
     * A comparison of two Date objects determining if the two objects are set to the same
     * day/date, independent of the time of day
     *
     * @param date1 A Date object
     * @param date2 A Date object
     * @return True if the Dates are set to the same day/date
     */
    public static boolean isSameDay(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance(), calendar2 = Calendar.getInstance();
        calendar1.setTime(date1);
        calendar2.setTime(date2);
        return isSameDay(calendar1, calendar2);
    }

    /**
     * Given a Date, calculates the number of days between now (the time at which the method is
     * called) and the given date
     *
     * @param date A Date object
     * @return The number of days between the current time and the given date
     */
    public static int daysAgo(Date date) {
        // 1000 * 60 * 60 * 24 is the number of milliseconds in a single day (1000 ms * 60 s *
        // 60 min * 24 hours).  The numerator is difference in milliseconds between now and the
        // given date.  The integer division of this is the number of days since 'date.'
        return (int) ((middleOfDay(new Date()).getTime() - middleOfDay(date).getTime()) /
                (1000L * 60L * 60L * 24L));
    }

    public static int daysBetween(Date date1, Date date2) {
        long difference = Math.abs(middleOfDay(date1).getTime() - middleOfDay(date2).getTime());
        return (int) (difference / (1000L * 60L * 60L * 24L));
    }

    /**
     * Formats a string representing the day of the week of the given date; "today" if today,
     * "yesterday" if yesterday, the day of the week's name (e.g. "Monday") if less than a week ago,
     * or the localized, shorthand (e.g. 11/7/2004) for all other cases
     *
     * @param date A Date object
     * @return An appropriately-formatted day-of-week string
     */
    public static String dayOfWeekOrDateString(Date date) {
        // If the date is today
        if (DateUtils.isToday(date.getTime()))
            return "Today";
        // Create a Calendar from the given date and increment it one day
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);
        // If the date is yesterday
        if (DateUtils.isToday(calendar.getTime().getTime()))
            return "Yesterday";
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, -7);
        if (date.compareTo(calendar.getTime()) < 0)
            return mDateFormat.format(date);
        // Revert the calendar and determine the day of the week
        calendar.setTime(date);
        int dayOfWeekAsInt = calendar.get(Calendar.DAY_OF_WEEK);
        switch (dayOfWeekAsInt) {
            case 1:
                return "Sunday";
            case 2:
                return "Monday";
            case 3:
                return "Tuesday";
            case 4:
                return "Wednesday";
            case 5:
                return "Thursday";
            case 6:
                return "Friday";
            default:
                return "Saturday";
        }
    }
}
