package luphi.swact.models;

import java.util.Date;

import luphi.swact.Utils;

/**
 * A representation of an individual weight record containing a timestamp, value, and optional note
 */
public class Weight {
    /**
     * Date, independent of the time of day, of the recording
     */
    public Date date;

    /**
     * Weight, in kilograms, of the recording
     */
    public float weight;

    /**
     * Optional text associated with this entry, entered by the user and displayed within the
     * history fragment/tab
     */
    public String note = null;

    /**
     * Constructor
     *
     * @param key A string representing a key used by this application's SharedPreferences, in the
     *            form "weight <timestamp>"
     * @param value A string representing a value used by this application's SharedPreferences, in
     *              the form "<float value> [<note string>]"
     */
    public Weight(String key, String value) {
        // Keys are in the form "weight 1274859304" for which the first integer is the
        // milliseconds since the Unix epoch
        String[] split = key.split(" ");
        date = Utils.middleOfDay(new Date(Long.parseLong(split[1])));
        // Values are formatted either in the form "155" for which the integer is just the weight
        // value, or "155 some arbitrary note" for which the string following the value is an
        // optional note
        split = value.split(" ");
        weight = Float.parseFloat(split[0]);
        if (split.length > 1)
            note = value.substring(value.indexOf(" ") + 1);
    }

    /**
     * Constructor
     *
     * @param date Date, independent of the time of day, of the recording
     * @param weight Weight, in kilograms, of the recording
     * @param note Optional text associated with this entry
     */
    public Weight(Date date, float weight, String note) {
        this.date = Utils.middleOfDay(date);
        this.weight = weight;
        this.note = note;
    }

    /**
     * Generates a string that may be used for storing this object in the application's
     * SharedPreferences and, by being passed to the constructor, recreate this instance
     *
     * @return A key to be used by SharedPreferences
     */
    public String key() {
        return "weight " + date.getTime();
    }

    /**
     * Generates a string that may be used for storing this object in the application's
     *      * SharedPreferences and, by being passed to the constructor, recreate this instance
     *
     * @return A value to be used by SharedPreferences
     */
    public String value() {
        return weight + (note != null ? " " + note : "");
    }

    /**
     * Compares two Weight objects and returns true if the two are equal or equivalent
     *
     * @param other Another object
     * @return True if equal or effectively equal
     */
    @Override
    public boolean equals(Object other) {
        // If an object is being compared to itself
        if (other == this)
            return true;
        // If the other object isn't even the same class
        if (!(other instanceof Weight))
            return false;
        Weight otherWeight = (Weight) other;
        // If the date or weight don't match
        if (!Utils.isSameDay(otherWeight.date, this.date) || (otherWeight.weight != this.weight))
            return false;
        // If the final attribute is null for both
        if ((otherWeight.note == null) && (this.note == null))
            return true;
        // At this point, at least one of the notes is not null so the note strings must be compared
        return (this.note != null && this.note.equals(otherWeight.note));
    }
}
