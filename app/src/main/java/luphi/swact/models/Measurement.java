package luphi.swact.models;

import java.util.Date;

import luphi.swact.Utils;

/**
 * A representation of an individual measurements record containing a timestamp, value, and optional
 * note
 */
public class Measurement {
    /**
     * Date, independent of the time of day, of the recording
     */
    public Date date;

    /**
     * The user-defined type this entry is associated with (e.g. "Biceps" for a bicep measurement)
     */
    public String type;

    /**
     * Value, without units, of the measurement
     */
    public float value;

    /**
     * Optional text associated with this entry, entered by the user and displayed within the
     * history fragment/tab
     */
    public String note = null;

    /**
     * Constructor
     *
     * @param key A string representing a key used by this application's SharedPreferences, in the
     *            form "measurements <timestamp>"
     * @param value A string representing a value used by this application's SharedPreferences, in
     *              the form "<type> <float value> [<note string>]"
     */
    public Measurement(String key, String value) {
        // Keys are in the form "measurements 1274859304" for which the first integer is the
        // milliseconds since the Unix epoch
        String[] split = key.split(" ");
        date = Utils.middleOfDay(new Date(Long.parseLong(split[1])));
        // Values are formatted either in the form "Biceps 14" for which the string is the type and
        // the integer is just the unit-agnostic value, or "Biceps 14 some arbitrary note" for which
        // the string following the value is an optional note
        split = value.split(" ");
        type = split[0];
        this.value = Float.parseFloat(split[1]);
        if (split.length > 2)
            note = value.substring(value.indexOf(" ", value.indexOf(" ") + 1) + 1);
    }

    /**
     * Constructor
     *
     * @param date Date, independent of the time of day, of the recording
     * @param type The user-defined type this entry is associated with
     * @param value Value, without units, of the measurement
     * @param note Optional text associated with this entry
     */
    public Measurement(Date date, String type, float value, String note) {
        this.date = Utils.middleOfDay(date);
        this.type = type;
        this.value = value;
        this.note = note;
    }

    /**
     * Generates a string that may be used for storing this object in the application's
     * SharedPreferences and, by being passed to the constructor, recreate this instance
     *
     * @return A key to be used by SharedPreferences
     */
    public String key() {
        return "measurements " + date.getTime();
    }

    /**
     * Generates a string that may be used for storing this object in the application's
     *      * SharedPreferences and, by being passed to the constructor, recreate this instance
     *
     * @return A value to be used by SharedPreferences
     */
    public String value() {
        return type + " " + value + (note != null ? " " + note : "");
    }

    /**
     * Compares two Measurement objects and returns true if the two are equal or equivalent
     *
     * @param other Another object
     * @return True if equal or effectively equal
     */
    @Override
    public boolean equals(Object other) {
        // If an object is being compared to itself
        if (other == this)
            return true;
        // If the other object isn't even the same class
        if (!(other instanceof Measurement))
            return false;
        Measurement otherMeasurement = (Measurement) other;
        // If the date or value don't match
        if (!Utils.isSameDay(otherMeasurement.date, this.date) ||
                (otherMeasurement.value != this.value))
            return false;
        // If the final attribute is null for both
        if ((otherMeasurement.note == null) && (this.note == null))
            return true;
        // At this point, at least one of the notes is not null so the note strings must be compared
        return (this.note != null && this.note.equals(otherMeasurement.note));
    }
}
