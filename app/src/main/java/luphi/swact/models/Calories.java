package luphi.swact.models;

import java.util.Date;

/**
 * A representation of an individual calorie record containing a timestamp, value, and optional note
 */
public class Calories {
    /**
     * Date and time of day at which the consumption was recorded
     */
    public Date date;

    /**
     * Calories, in kilocalories, consumed
     */
    public int calories;

    /**
     * Optional text associated with this entry, entered by the user and displayed within the
     * history fragment/tab
     */
    public String note = null;

    /**
     * Constructor
     *
     * @param key A string representing a key used by this application's SharedPreferences, in the
     *            form "calories <timestamp>"
     * @param value A string representing a value used by this application's SharedPreferences, in
     *              the form "<integer value> [<note string>]"
     */
    public Calories(String key, String value) {
        // Keys are in the form "calories 1274859304" for which the first integer is the
        // milliseconds since the Unix epoch
        String[] split = key.split(" ");
        date = new Date(Long.parseLong(split[1]));
        // Values are formatted either in the form "965" for which the integer is just the caloric
        // value, or "965 some arbitrary note" for which the string following the value is an
        // optional note
        split = value.split(" ");
        calories = Integer.parseInt(split[0]);
        if (split.length > 1)
            note = value.substring(value.indexOf(" ") + 1);
    }

    /**
     * Constructor
     *
     * @param date Date and time of day at which the consumption was recorded
     * @param calories Calories, in kilocalories, consumed
     * @param note Optional text associated with this entry
     */
    public Calories(Date date, int calories, String note) {
        this.date = date;
        this.calories = calories;
        this.note = note;
    }

    /**
     * Generates a string that may be used for storing this object in the application's
     * SharedPreferences and, by being passed to the constructor, recreate this instance
     *
     * @return A key to be used by SharedPreferences
     */
    public String key() {
        return "calories " + date.getTime();
    }

    /**
     * Generates a string that may be used for storing this object in the application's
     *      * SharedPreferences and, by being passed to the constructor, recreate this instance
     *
     * @return A value to be used by SharedPreferences
     */
    public String value() {
        return calories + (note != null ? " " + note : "");
    }

    /**
     * Compares two Calories objects and returns true if the two are equal or equivalent
     *
     * @param other Another object
     * @return True if equal or effectively equal
     */
    @Override
    public boolean equals(Object other) {
        // If an object is being compared to itself
        if (other == this)
            return true;
        // If the other object isn't even the same class
        if (!(other instanceof Calories))
            return false;
        Calories otherCalories = (Calories) other;
        // If the date or calories consumed don't match
        if (!otherCalories.date.equals(this.date) || (otherCalories.calories != this.calories))
            return false;
        // If the final attribute is null for both
        if ((otherCalories.note == null) && (this.note == null))
            return true;
        // At this point, at least one of the notes is not null so the note strings must be compared
        return (this.note != null && this.note.equals(otherCalories.note));
    }
}
