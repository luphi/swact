package luphi.swact.views.weight;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.util.Locale;

import luphi.swact.R;
import luphi.swact.Record;
import luphi.swact.Utils;
import luphi.swact.views.SWaCTFragment;

/**
 * A fragment hosted in the weight drawer containing a body mass index calculator
 */
public class WeightBmiFragment extends SWaCTFragment {
    /**
     * Called by the system to create the view
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.weight_bmi, container, false);
    }

    /**
     * Called by the system following view creation; adds styling to the pie chart for showing and
     * highlighting the four categories
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getView() != null) {
            TextView bmiText = getView().findViewById(R.id.weight_bmi_bmi_label);
            bmiText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getContext(), getResources().getString(
                            R.string.weight_bmi_bmi_explanation), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    /**
     * Called whenever this fragment is shown; fills out and shows views depending on whether all
     * necessary information is present or not, including the BMI value (by calculating it)
     */
    @Override
    public void refresh() {
        if (getView() != null) {
            TextView bmiValueText = getView().findViewById(R.id.weight_bmi_bmi_value),
                    footText = getView().findViewById(R.id.weight_bmi_feet),
                    inchText = getView().findViewById(R.id.weight_bmi_inches),
                    centimetersText = getView().findViewById(R.id.weight_bmi_centimeters),
                    kilogramsText = getView().findViewById(R.id.weight_bmi_kilograms),
                    poundsText = getView().findViewById(R.id.weight_bmi_pounds);
            View imperialHeightLayout =
                    getView().findViewById(R.id.weight_bmi_imperial_height_layout),
                    metricHeightLayout =
                            getView().findViewById(R.id.weight_bmi_metric_height_layout),
                    imperialWeightLayout =
                            getView().findViewById(R.id.weight_bmi_imperial_weight_layout),
                    metricWeightLayout =
                            getView().findViewById(R.id.weight_bmi_metric_weight_layout),
                    needMoreInfoLayout =
                            getView().findViewById(R.id.weight_bmi_need_more_info_cardview),
                    knownInfoLayout =
                            getView().findViewById(R.id.weight_bmi_known_input_data_cardview);
            resetBmiTableRowPadding();
            if (isAllInputPresent()) {
                needMoreInfoLayout.setVisibility(View.GONE);
                knownInfoLayout.setVisibility(View.VISIBLE);
                float bmi = Record.mostRecentWeight() / (float) Math.pow(Record.height / 100f, 2f);
                LinearLayout rowLayout;
                if (bmi < 18.5f)
                    rowLayout = getView().findViewById(R.id.weight_bmi_underweight_row);
                else if ((bmi >= 18.5f) && (bmi < 25f))
                    rowLayout = getView().findViewById(R.id.weight_bmi_normal_weight_row);
                else if ((bmi >= 25f) && (bmi < 30f))
                    rowLayout = getView().findViewById(R.id.weight_bmi_overweight_row);
                else if ((bmi >= 30f) && (bmi < 35f))
                    rowLayout = getView().findViewById(R.id.weight_bmi_obese_class_1_row);
                else if ((bmi >= 35f) && (bmi < 40f))
                    rowLayout = getView().findViewById(R.id.weight_bmi_obese_class_2_row);
                else
                    rowLayout = getView().findViewById(R.id.weight_bmi_obese_class_3_row);
                int horizontalPadding = (int)
                        getResources().getDimension(R.dimen.bmi_table_row_horizontal_padding),
                        verticalPadding = (int)
                        getResources().getDimension(R.dimen.bmi_table_row_vertical_padding);
                rowLayout.setPadding(horizontalPadding, verticalPadding * 2,
                        horizontalPadding, verticalPadding * 2);
                if (Record.useImperialHeight) {
                    imperialHeightLayout.setVisibility(View.VISIBLE);
                    metricHeightLayout.setVisibility(View.GONE);
                    footText.setText(String.valueOf(Utils.centimetersToInches(Record.height) / 12));
                    inchText.setText(String.valueOf(Utils.centimetersToInches(Record.height) % 12));
                }
                else {
                    imperialHeightLayout.setVisibility(View.GONE);
                    metricHeightLayout.setVisibility(View.VISIBLE);
                    centimetersText.setText(String.valueOf(Record.height));
                }
                if (Record.useImperialWeight) {
                    imperialWeightLayout.setVisibility(View.VISIBLE);
                    metricWeightLayout.setVisibility(View.GONE);
                    poundsText.setText(String.format(Locale.getDefault(), "%.1f",
                            Utils.kilogramsToPounds(Record.mostRecentWeight())));
                }
                else {
                    imperialWeightLayout.setVisibility(View.GONE);
                    metricWeightLayout.setVisibility(View.VISIBLE);
                    kilogramsText.setText(String.format(Locale.getDefault(), "%.1f",
                            Record.mostRecentWeight()));
                }
                bmiValueText.setText(String.format(Locale.getDefault(), "%.1f", bmi));
            }
            else {
                needMoreInfoLayout.setVisibility(View.VISIBLE);
                knownInfoLayout.setVisibility(View.GONE);
                bmiValueText.setText("?");
            }
        }
    }

    /**
     * A helper method for determining if all input from the user needed by this fragment's
     * calculator is present
     *
     * @return True if all the data needed to calculate the BMI has been provided
     */
    private boolean isAllInputPresent() {
        if (Record.height == 0)
            return false;
        return Record.mostRecentWeight() != 0;
    }

    /**
     * A helper function to return the BMI table rows to their default padding
     */
    private void resetBmiTableRowPadding() {
        if (getView() != null) {
            int horizontalPadding = (int)
                    getResources().getDimension(R.dimen.bmi_table_row_horizontal_padding),
                    verticalPadding = (int)
                    getResources().getDimension(R.dimen.bmi_table_row_vertical_padding);
            getView().findViewById(R.id.weight_bmi_underweight_row).setPadding(
                    horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
            getView().findViewById(R.id.weight_bmi_normal_weight_row).setPadding(
                    horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
            getView().findViewById(R.id.weight_bmi_overweight_row).setPadding(
                    horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
            getView().findViewById(R.id.weight_bmi_obese_class_1_row).setPadding(
                    horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
            getView().findViewById(R.id.weight_bmi_obese_class_2_row).setPadding(
                    horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
            getView().findViewById(R.id.weight_bmi_obese_class_3_row).setPadding(
                    horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
        }
    }
}
