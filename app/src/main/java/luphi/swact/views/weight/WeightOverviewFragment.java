package luphi.swact.views.weight;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import luphi.swact.Utils;
import luphi.swact.SWaCTActivity;
import luphi.swact.R;
import luphi.swact.Record;
import luphi.swact.models.Weight;
import luphi.swact.views.SWaCTFragment;

/**
 * A fragment hosted in the weight drawer containing a chart, relevant information, and a button
 * for adding new entries
 */
public class WeightOverviewFragment extends SWaCTFragment {
    /**
     * A copy of the last known weight data, used to determine if the graph should be refreshed
     */
    private List<Weight> mLastKnownData = null;

    /**
     * A copy of the last known weight goal, used to determine if the graph should be refreshed
     */
    private float mLastKnownGoal = 0f;

    /**
     * A string copy of the last known "use imperial weight" flag (referring to weight units); in
     * the form of a string so that it may hold three values: null, "true," or "false"
     */
    private String mLastKnownUseImperialWeight = null;

    /**
     * Called by the system to create the view
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.weight_overview, container, false);
    }

    /**
     * Called by the system following view creation; sets up the chart's styling and creates a
     * callback for the add button
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getView() != null) {
            LineChart lineChart = getView().findViewById(R.id.weight_overview_chart);
            if (lineChart.getData() != null)
                lineChart.getData().clearValues();
            lineChart.notifyDataSetChanged();
            lineChart.clear();
            lineChart.invalidate();
            lineChart.setNoDataTextColor(getResources().getColor(R.color.color_accent));
            // Disable multiple things that are unwanted (background grid, individual value
            // selection, the chart description, the right-hand-side Y axis, and legend)
            lineChart.setDrawGridBackground(false);
            lineChart.setHighlightPerDragEnabled(false);
            lineChart.setHighlightPerTapEnabled(false);
            lineChart.getDescription().setEnabled(false);
            lineChart.getAxisRight().setEnabled(false);
            lineChart.getLegend().setEnabled(false);
            // Disable the Y axis grid
            lineChart.getAxisLeft().setDrawGridLines(false);
            XAxis xAxis = lineChart.getXAxis();
            // Move the X axis to the bottom and disable its grid
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setDrawAxisLine(false);
            xAxis.setDrawGridLines(false);
            xAxis.setValueFormatter(new ValueFormatter() {
                private DateFormat mFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());
                @Override
                public String getFormattedValue(float value) {
                    // Entry X values are dates in the format of a float for which their values are
                    // the count of milliseconds since the Unix epoch.  The format used here returns
                    // a string akin to "20 Nov" indicating the day and month.
                    return mFormat.format(new Date((long) value));
                }
            });
            FloatingActionButton fab = getView().findViewById(R.id.weight_overview_floating_button);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getActivity() != null)
                        ((SWaCTActivity) getActivity()).addWeight();
                }
            });
            lineChart.invalidate(); // Refresh the chart
        }
    }

    /**
     * Called whenever this fragment is shown; refreshes the chart by generating an entry list and
     * applying styling to it, and fills out the daily and goal weight views
     */
    @Override
    public void refresh() {
        if (getView() != null) {
            LineChart lineChart = getView().findViewById(R.id.weight_overview_chart);
            // True if weight records were changed or this fragment is newly created
            boolean isDataModified = (mLastKnownData == null) ||
                    (!mLastKnownData.equals(Record.Weights)),
                    // True if the goal weight has been modified
                    isGoalModified = mLastKnownGoal != Record.goalWeight,
                    // True if the preferred weight units (pounds or kilograms) has changed
                    areUnitsModified = (mLastKnownUseImperialWeight == null) ||
                            !mLastKnownUseImperialWeight.equals(String.valueOf(
                                    Record.useImperialWeight));
            if (isDataModified || isGoalModified || areUnitsModified) {
                // If a copy of the records should be made
                if (isDataModified)
                    mLastKnownData = new LinkedList<>(Record.Weights);
                // If a copy of the goal weight should be made
                if (isGoalModified)
                    mLastKnownGoal = Record.goalWeight;
                //If a copy of the preferred units should be made
                if (areUnitsModified)
                    mLastKnownUseImperialWeight = String.valueOf(Record.useImperialWeight);
                if (Record.Weights.size() > 0) {
                    int entriesThisWeek = 0;
                    List<Entry> entries = new LinkedList<>();
                    for (Weight weight : Record.Weights) {
                        if (Utils.daysAgo(weight.date) < 7)
                            entriesThisWeek += 1;
                        float weightValue = weight.weight;
                        if (Record.useImperialWeight)
                            weightValue = Utils.kilogramsToPounds(weightValue);
                        entries.add(new Entry(weight.date.getTime(), weightValue));
                    }
                    // Sort the entries by date, in ascending order
                    Collections.reverse(entries);
                    LineDataSet lineDataSet = new LineDataSet(entries, null);
                    // Apply some styling to the data (line width 3, line color, circle [value]
                    // color, and circle size[s])
                    lineDataSet.setLineWidth(4f);
                    lineDataSet.setColor(getResources().getColor(R.color.color_accent));
                    lineDataSet.setCircleColor(getResources().getColor(R.color.color_accent));
                    lineDataSet.setCircleRadius(4f);
                    lineDataSet.setCircleHoleRadius(3f);
                    lineDataSet.setDrawValues(true);
                    if (mIsNightModeOn)
                        lineDataSet.setValueTextColor(Color.WHITE);
                    else
                        lineDataSet.setValueTextColor(Color.BLACK);
                    lineChart.setData(new LineData(lineDataSet));
                    // Set the visible X range maximum to the middle of the current day
                    lineChart.getXAxis().setAxisMaximum((float)
                            Utils.middleOfDay(new Date()).getTime());
                    // If the oldest entry is less than 7 days (a week) ago
                    if (Utils.daysAgo(Record.Weights.get(Record.Weights.size() - 1).date) < 7) {
                        // Disable user input because there's really no point with so little data
                        lineChart.setTouchEnabled(false);
                        // Limit the number of X axis labels to match the number of days and limit
                        // the visible X range minimum to the width of one day (86400000 ms)
                        lineChart.getXAxis().setLabelCount(entriesThisWeek, true);
                        lineChart.setVisibleXRangeMinimum(86400000f);
                        // With input being disabled, autoscaling isn't needed
                        lineChart.setAutoScaleMinMaxEnabled(false);
                    } else {
                        //Enable user input so he/she can manipulate the viewport (visible data)
                        lineChart.setTouchEnabled(true);
                        // Limit the number of X axis labels a week's worth of days and limit the
                        // visible X range minimum to the width of seven points (86400000 * 6 ms)
                        lineChart.getXAxis().setLabelCount(7, true);
                        lineChart.setVisibleXRangeMinimum(86400000f * 6f);
                        // Restrict the Y axis to values in the visible X range.  The Y axis will
                        // scale when the user adjusts the X axis.  Zooming in on the Y axis is
                        // still possible.
                        lineChart.setAutoScaleMinMaxEnabled(true);
                    }
                    // If the oldest entry is more than 30 days (~a month) ago
                    if (Utils.daysAgo(Record.Weights.get(Record.Weights.size() - 1).date) > 30) {
                        // Zoom in an excessive amount and allow the chart to fit itself to 30 days
                        // worth of entries, then return the minimum range to seven days worth
                        lineChart.setVisibleXRangeMinimum(86400000f * 29f);
                        lineChart.zoom(1000f, 1f, 0f, 0f);
                        lineChart.setVisibleXRangeMinimum(86400000f * 6f);
                        // moveViewToX() is intended to set the left edge of the viewport but, to
                        // make our lives easier, we'll set it to the right-most data point and let
                        // the chart fit itself
                        lineChart.moveViewToX(new Date().getTime());
                    }
                }
                else
                    lineChart.setData(null);
                if (mIsNightModeOn) {
                    lineChart.setNoDataTextColor(Color.WHITE);
                    lineChart.getXAxis().setTextColor(Color.WHITE);
                    lineChart.getAxisLeft().setTextColor(Color.WHITE);
                }
                else {
                    lineChart.setNoDataTextColor(Color.BLACK);
                    lineChart.getXAxis().setTextColor(Color.BLACK);
                    lineChart.getAxisLeft().setTextColor(Color.BLACK);
                }
                lineChart.invalidate();
            }
            if (Record.goalWeight > 0f) {
                YAxis yAxis = lineChart.getAxisLeft();
                yAxis.removeAllLimitLines();
                float goalWeight = Record.goalWeight;
                if (Record.useImperialWeight)
                    goalWeight = Utils.kilogramsToPounds(goalWeight);
                LimitLine limitLine = new LimitLine(goalWeight);
                limitLine.enableDashedLine(20f, 20f, 0);
                limitLine.setLineWidth(2f);
                yAxis.addLimitLine(limitLine);
            }
            TextView todaysWeightText = getView().findViewById(R.id.weight_overview_todays_weight),
                    todaysWeightUnitsText =
                            getView().findViewById(R.id.weight_overview_todays_weight_units),
                    goalWeightText = getView().findViewById(R.id.weight_overview_goal_weight),
                    goalWeightUnitsText =
                            getView().findViewById(R.id.weight_overview_goal_weight_units);
            float todaysWeight = Record.todaysWeight();
            if (Record.useImperialWeight)
                todaysWeight = Utils.kilogramsToPounds(todaysWeight);
            if (todaysWeight == 0f)
                todaysWeightText.setText("?");
            else
                todaysWeightText.setText(String.format(Locale.getDefault(), "%.1f", todaysWeight));
            if (Utils.fuzzyEquals(Record.goalWeight, 0f))
                goalWeightText.setText("?");
            else {
                float goalWeight = Record.goalWeight;
                if (Record.useImperialWeight)
                    goalWeight = Utils.kilogramsToPounds(goalWeight);
                goalWeightText.setText(String.format(Locale.getDefault(), "%.1f", goalWeight));
            }
            String units;
            if (Record.useImperialWeight)
                units = getResources().getString(R.string.imperial_weight_units);
            else
                units = getResources().getString(R.string.metric_weight_units);
            todaysWeightUnitsText.setText(units);
            goalWeightUnitsText.setText(units);
        }
    }
}
