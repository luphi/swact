package luphi.swact.views.weight;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;

import java.util.LinkedList;
import java.util.List;

import luphi.swact.R;
import luphi.swact.Record;
import luphi.swact.adapters.WeightAdapter;
import luphi.swact.models.Weight;
import luphi.swact.views.SWaCTFragment;

/**
 * A fragment hosted in the weight drawer containing a list (history) of weight recordings
 */
public class WeightHistoryFragment extends SWaCTFragment {
    /**
     * An adapter for wrapping weight recordings and creating their list item view
     */
    private WeightAdapter mWeightAdapter = null;

    /**
     * A copy of the last known weight data, used to determine if the list should be refreshed
     */
    private List<Weight> mLastKnownData = null;

    /**
     * A copy of the last known weight goal, used to determine if the list should be refreshed
     */
    private float mLastKnownGoal = 0f;

    /**
     * A string copy of the last known "use imperial weight" flag (referring to weight units); in
     * the form of a string so that it may hold three values: null, "true," or "false"
     */
    private String mLastKnownUseImperialWeight = null;

    /**
     * Called by the system to create the view
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.weight_history, container, false);
    }

    /**
     * Called by the system following view creation; attaches the adapter to the list
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getView() != null) {
            ListView listView = getView().findViewById(R.id.weight_history_list);
            mWeightAdapter = new WeightAdapter(getContext(), mIsNightModeOn);
            listView.setAdapter(mWeightAdapter);
        }
    }

    /**
     * Called whenever this fragment is shown; refreshes the list if appropriate
     */
    @Override
    public void refresh() {
        if (getView() != null) {
            View noHistoryText = getView().findViewById(R.id.weight_history_no_history_label);
            ListView historyList = getView().findViewById(R.id.weight_history_list);
            // If there are no list items to display
            if (Record.Weights.isEmpty()) {
                // Display the "no history" notice and hide the list
                noHistoryText.setVisibility(View.VISIBLE);
                historyList.setVisibility(View.GONE);
            } else {
                // Display the list and hide the "no history" notice
                noHistoryText.setVisibility(View.GONE);
                historyList.setVisibility(View.VISIBLE);
            }
            // True if weight records were changed or this fragment is newly created
            boolean isDataModified = (mLastKnownData == null) ||
                    !mLastKnownData.equals(Record.Weights),
                    // True if the goal weight has been modified
                    isGoalModified = mLastKnownGoal != Record.goalWeight,
                    // True if the preferred weight units (pounds or kilograms) has changed
                    areUnitsModified = (mLastKnownUseImperialWeight == null) ||
                            !mLastKnownUseImperialWeight.equals(String.valueOf(
                                    Record.useImperialWeight));
            // If a copy of the records should be made
            if (isDataModified)
                mLastKnownData = new LinkedList<>(Record.Weights);
            // If a copy of the goal weight should be made
            if (isGoalModified)
                mLastKnownGoal = Record.goalWeight;
            //If a copy of the preferred units should be made
            if (areUnitsModified)
                mLastKnownUseImperialWeight = String.valueOf(Record.useImperialWeight);
            // If the list should be refreshed
            if (isDataModified || isGoalModified || areUnitsModified || isNewDay()) {
                mWeightAdapter = new WeightAdapter(getContext(), mIsNightModeOn);
                historyList.setAdapter(mWeightAdapter);
            }
        }
    }
}
