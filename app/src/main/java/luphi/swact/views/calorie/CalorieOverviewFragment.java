package luphi.swact.views.calorie;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import luphi.swact.SWaCTActivity;
import luphi.swact.R;
import luphi.swact.Record;
import luphi.swact.Utils;
import luphi.swact.models.Calories;
import luphi.swact.views.SWaCTFragment;

/**
 * A fragment hosted in the calories drawer containing a chart, relevant information, and a button
 * for adding new entries
 */
public class CalorieOverviewFragment extends SWaCTFragment {
    /**
     * A copy of the last known calorie data, used to determine if the chart should be refreshed
     */
    private List<Calories> mLastKnownData = null;

    /**
     * A copy of the last known daily caloric goal, used to determine if the chart should be
     * refreshed
     */
    private float mLastKnownGoal = 0f;

    /**
     * Called by the system to create the view
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.calories_overview, container, false);
    }

    /**
     * Called by the system following view creation; sets up the chart's styling and creates a
     * callback for the add button
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getView() != null) {
            BarChart barChart = getView().findViewById(R.id.calories_overview_chart);
            barChart.setNoDataTextColor(getResources().getColor(R.color.color_accent));
            // Disable multiple things that are unwanted (background grid, individual value
            // selection, the chart description, the right-hand-side Y axis, and legend)
            barChart.setDrawGridBackground(false);
            barChart.setHighlightPerDragEnabled(false);
            barChart.setHighlightPerTapEnabled(false);
            barChart.getDescription().setEnabled(false);
            barChart.getAxisRight().setEnabled(false);
            barChart.getLegend().setEnabled(false);
            barChart.setFitBars(true);
            // Disable the Y axis grid
            barChart.getAxisLeft().setDrawGridLines(false);
            // Move the X axis to the bottom and disable its grid
            XAxis xAxis = barChart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setDrawAxisLine(false);
            xAxis.setDrawGridLines(false);
            xAxis.setValueFormatter(new ValueFormatter() {
                private DateFormat mFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());
                @Override
                public String getFormattedValue(float value) {
                    // Entry X values are dates in the format of a float for which their values are
                    // the count of milliseconds since the Unix epoch.  The format used here returns
                    // a string akin to "20 Nov" indicating the day and month.
                    return mFormat.format(new Date((long) value));
                }
            });
            //barChart.invalidate(); // Refresh the chart
            FloatingActionButton fab =
                    getView().findViewById(R.id.calories_overview_floating_button);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getActivity() != null)
                        ((SWaCTActivity) getActivity()).addCalories();
                }
            });
        }
    }

    /**
     * Called whenever this fragment is shown; refreshes the chart by generating an entry list and
     * applying styling to it, and fills out the daily and goal calorie views
     */
    @Override
    public void refresh() {
        if (getView() != null) {
            BarChart barChart = getView().findViewById(R.id.calories_overview_chart);
            if ((mLastKnownData == null) || !mLastKnownData.equals(Record.Calories)) {
                mLastKnownData = new LinkedList<>(Record.Calories);
                if (!Record.Calories.isEmpty()) {
                    List<BarEntry> entries = new LinkedList<>();
                    int dailyCalories = 0, daysWithEntriesThisWeek = 0;
                    Date dayDate = Record.Calories.get(0).date;
                    // If this entry is less than a week old
                    if (Utils.daysAgo(dayDate) < 7)
                        daysWithEntriesThisWeek += 1;
                    for (Calories calories : Record.Calories) {
                        // If this record is from a different day
                        if (!Utils.isSameDay(dayDate, calories.date)) {
                            entries.add(new BarEntry(Utils.middleOfDay(dayDate).getTime(),
                                    dailyCalories));
                            dailyCalories = 0;
                            dayDate = calories.date;
                            // If this entry is less than a week old
                            if (Utils.daysAgo(dayDate) < 7)
                                daysWithEntriesThisWeek += 1;
                        }
                        dailyCalories += calories.calories;
                    }
                    // Add the final entry.  Adding entries to the list is done in the above loop
                    // when a new day is found.  For the final summation, there will be no new day.
                    entries.add(new BarEntry(Utils.middleOfDay(dayDate).getTime(), dailyCalories));
                    // Sort the entries by date, in ascending order
                    Collections.reverse(entries);
                    BarDataSet barDataSet = new BarDataSet(entries, null);
                    barDataSet.setColor(getResources().getColor(R.color.color_accent));
                    BarData barData = new BarData(barDataSet);
                    if (mIsNightModeOn)
                        barDataSet.setValueTextColor(Color.WHITE);
                    else
                        barDataSet.setValueTextColor(Color.BLACK);
                    barChart.setData(barData);
                    // Set the visible X range maximum to the start of the next day
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(Utils.startOfDay(new Date()));
                    calendar.add(Calendar.DATE, 1);
                    barChart.getXAxis().setAxisMaximum((float) calendar.getTimeInMillis());
                    // If the oldest entry is less than 7 days (a week) ago
                    if (Utils.daysAgo(Record.Calories.get(Record.Calories.size() - 1).date) < 7) {
                        // Disable user input because there's really no point with so little data
                        barChart.setTouchEnabled(false);
                        // Limit the number of X axis labels to match the number of days and limit
                        // the visible X range minimum to the width of one day (86400000 ms)
                        barChart.getXAxis().setLabelCount(daysWithEntriesThisWeek, true);
                        barChart.setVisibleXRangeMinimum(86400000f);
                        // With input being disabled, autoscaling isn't needed
                        barChart.setAutoScaleMinMaxEnabled(false);
                    } else {
                        //Enable user input so he/she can manipulate the viewport (visible data)
                        barChart.setTouchEnabled(true);
                        // Limit the number of X axis labels a week's worth of days
                        XAxis xAxis = barChart.getXAxis();
                        xAxis.setLabelCount(7, true);
                        // Set the visible X range minimum to the start of the day of the oldest
                        // entry
                        xAxis.setAxisMinimum((float) Utils.startOfDay(
                                Record.Calories.get(Record.Calories.size() - 1).date).getTime());
                        // Limit the visible X range minimum to the width of seven bars
                        // (86400000 * 7 ms)
                        barChart.setVisibleXRangeMinimum(86400000f * 7f);
                        // Restrict the Y axis to values in the visible X range.  The Y axis will
                        // scale when the user adjusts the X axis.  Zooming in on the Y axis is
                        // still possible.
                        barChart.setAutoScaleMinMaxEnabled(true);
                    }
                    // If the oldest entry is more than 30 days (~a month) ago
                    if (Utils.daysAgo(Record.Calories.get(Record.Calories.size() - 1).date) > 30) {
                        // Zoom in an excessive amount and allow the chart to fit itself to 30 days
                        // worth of entries, then return the minimum range to seven days worth
                        barChart.setVisibleXRangeMinimum(86400000f * 30f);
                        barChart.zoom(1000f, 1f, 0f, 0f);
                        barChart.setVisibleXRangeMinimum(86400000f * 7f);
                        // moveViewToX() is intended to set the left edge of the viewport but, to
                        // make our lives easier, we'll set it to the right-most data point and let
                        // the chart fit itself
                        barChart.moveViewToX(new Date().getTime());
                    }
                    // Due to using millisecond X values, there is a large (86400000f ms) difference
                    // between each data point and the charts calculate bar width relative to that
                    // difference.  If not set manually, the bar width will be too small to see.
                    barData.setBarWidth(86400000f * 0.9f);
                }
                else
                    barChart.setData(null);
                if (mIsNightModeOn) {
                    barChart.setNoDataTextColor(Color.WHITE);
                    barChart.getXAxis().setTextColor(Color.WHITE);
                    barChart.getAxisLeft().setTextColor(Color.WHITE);
                }
                else {
                    barChart.setNoDataTextColor(Color.BLACK);
                    barChart.getXAxis().setTextColor(Color.BLACK);
                    barChart.getAxisLeft().setTextColor(Color.BLACK);
                }
                barChart.invalidate();
            }
            if ((mLastKnownGoal == 0f) || (mLastKnownGoal != Record.goalCalories))  {
                mLastKnownGoal = Record.goalCalories;
                if (Record.goalCalories > 0f) {
                    YAxis yAxis = barChart.getAxisLeft();
                    yAxis.removeAllLimitLines();
                    LimitLine limitLine = new LimitLine(Record.goalCalories);
                    limitLine.enableDashedLine(20f, 20f, 0);
                    limitLine.setLineWidth(2f);
                    yAxis.addLimitLine(limitLine);
                }
            }
            TextView todaysCaloriesText =
                    getView().findViewById(R.id.calories_overview_todays_calories),
                    goalCaloriesText = getView().findViewById(R.id.calories_overview_goal_calories),
                    toGoalCaloriesText =
                            getView().findViewById(R.id.calories_overview_to_goal_calories);
            int todaysCalories = Record.todaysCalories();
            todaysCaloriesText.setText(String.valueOf(todaysCalories));
            if (Record.goalCalories > 0) {
                goalCaloriesText.setText(String.valueOf(Record.goalCalories));
                toGoalCaloriesText.setText(String.valueOf(Record.goalCalories - todaysCalories));
            }
            else {
                goalCaloriesText.setText("?");
                toGoalCaloriesText.setText("?");
            }
        }
    }
}
