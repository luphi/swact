package luphi.swact.views.calorie;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.util.Locale;

import luphi.swact.R;
import luphi.swact.Record;
import luphi.swact.Utils;
import luphi.swact.views.SWaCTFragment;

/**
 * A fragment hosted in the calories drawer containing a total daily energy expenditure calculator
 */
public class CalorieTdeeFragment extends SWaCTFragment {
    /**
     * Called by the system to create the view
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.calories_tdee, container, false);
    }

    /**
     * Called by the system following view creation; adds callbacks to various views for showing
     * tooltips or actually performing the calculation
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getView() != null) {
            final TextView activityLevelText =
                    getView().findViewById(R.id.calories_tdee_activity_level_label),
                    bmrLabelText = getView().findViewById(R.id.calories_tdee_bmr_label),
                    bmrValueText = getView().findViewById(R.id.calories_tdee_bmr_value),
                    tdeeLabelText = getView().findViewById(R.id.calories_tdee_tdee_label),
                    tdeeValueText = getView().findViewById(R.id.calories_tdee_tdee_value);
            Spinner activityLevelSpinner =
                    getView().findViewById(R.id.calories_tdee_activity_level_spinner);
            activityLevelText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getContext(), getResources().getString(
                            R.string.calories_tdee_activity_level_explanation), Toast.LENGTH_LONG).
                            show();
                }
            });
            bmrLabelText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getContext(), getResources().getString(
                            R.string.calories_tdee_bmr_explanation), Toast.LENGTH_LONG).show();
                }
            });
            tdeeLabelText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getContext(), getResources().getString(
                            R.string.calories_tdee_tdee_explanation), Toast.LENGTH_LONG).show();
                }
            });
            activityLevelSpinner.setSelection(Record.activityLevel);
            activityLevelSpinner.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i,
                                                   long l) {
                            Record.activityLevel = i;
                            Record.write(getContext(), "activity_level", i);
                            if (!isAllInputPresent())
                                return;
                            int bmr = harrisBenedictBMR();
                            bmrValueText.setText(String.valueOf(bmr));
                            tdeeValueText.setText(String.format(Locale.getDefault(), "%.0f",
                                    bmr * activityLevelFactor()));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });
        }
    }

    /**
     * Called whenever this fragment is shown; fills out and shows views depending on whether all
     * necessary information is present or not
     */
    @Override
    public void refresh() {
        if (getView() != null) {
            View imperialHeightLayout =
                    getView().findViewById(R.id.calories_tdee_imperial_height_layout),
                    metricHeightLayout =
                            getView().findViewById(R.id.calories_tdee_metric_height_layout),
                    needMoreInfoLayout =
                    getView().findViewById(R.id.calories_tdee_need_more_info_cardview),
                    knownInfoLayout =
                            getView().findViewById(R.id.calories_tdee_known_input_data_cardview);
            TextView sexText = getView().findViewById(R.id.calories_tdee_sex),
                    ageText = getView().findViewById(R.id.calories_tdee_age),
                    feetText = getView().findViewById(R.id.calories_tdee_feet),
                    inchesText = getView().findViewById(R.id.calories_tdee_inches),
                    centimetersText = getView().findViewById(R.id.calories_tdee_centimeters),
                    poundsText = getView().findViewById(R.id.calories_tdee_pounds),
                    kilogramsText = getView().findViewById(R.id.calories_tdee_kilograms),
                    bmrValueText = getView().findViewById(R.id.calories_tdee_bmr_value),
                    tdeeValueText = getView().findViewById(R.id.calories_tdee_tdee_value);
            if (isAllInputPresent()) {
                needMoreInfoLayout.setVisibility(View.GONE);
                knownInfoLayout.setVisibility(View.VISIBLE);
                sexText.setText(Record.sex.equals("male") ? "M" : "F");
                ageText.setText(String.valueOf(Record.age));
                if (Record.useImperialHeight) {
                    imperialHeightLayout.setVisibility(View.VISIBLE);
                    metricHeightLayout.setVisibility(View.GONE);
                    feetText.setText(String.valueOf(Utils.centimetersToInches(Record.height) / 12));
                    inchesText.setText(String.valueOf(Utils.centimetersToInches(Record.height) % 12));
                    poundsText.setText(String.format(Locale.getDefault(), "%.1f",
                            Utils.kilogramsToPounds(Record.mostRecentWeight())));
                }
                else {
                    imperialHeightLayout.setVisibility(View.GONE);
                    metricHeightLayout.setVisibility(View.VISIBLE);
                    centimetersText.setText(String.valueOf(Record.height));
                    kilogramsText.setText(String.format(Locale.getDefault(), "%.1f",
                            Record.mostRecentWeight()));
                }
                int bmr = harrisBenedictBMR();
                bmrValueText.setText(String.valueOf(bmr));
                tdeeValueText.setText(String.format(Locale.getDefault(), "%.0f",
                        bmr * activityLevelFactor()));
            }
            else {
                needMoreInfoLayout.setVisibility(View.VISIBLE);
                knownInfoLayout.setVisibility(View.GONE);
                bmrValueText.setText("?");
                tdeeValueText.setText("?");
            }
        }
    }

    /**
     * A helper method for determining if all input from the user needed by this fragment's
     * calculator is present
     *
     * @return True if all the data needed to calculate the TDEE has been provided
     */
    private boolean isAllInputPresent() {
        if ((Record.sex == null) || (!Record.sex.equals("male") && !Record.sex.equals("female")))
            return false;
        if (Record.age == 0)
            return false;
        if (Record.height == 0)
            return false;
        return Record.mostRecentWeight() != 0;
    }

    /**
     * Using information present in records, calculates the user's basal metabolic rate using the
     * Harris-Benedict equation
     *
     * @return The user's basal metabolic rate in kilocalories
     */
    private int harrisBenedictBMR() {
        // Male BMR = 66 + (13.7 x weight in kg) + (5 x height in cm) - (6.8 x age in years)
        // Female BMR = 655 + (9.6 x weight in kg) + (1.8 x height in cm) - (4.7 x age in years)
        float constant, weightFactor, heightFactor, ageFactor;
        // Error check, should be impossible
        if (Record.sex == null)
            return 0;
        // If the user is male
        if (Record.sex.equals("male")) {
            constant = 66f;
            weightFactor = 13.7f;
            heightFactor = 5f;
            ageFactor = 6.8f;
        }
        // If the user is female
        else {
            constant = 655f;
            weightFactor = 9.6f;
            heightFactor = 1.8f;
            ageFactor = 4.7f;
        }
        return (int) (constant + (weightFactor * Record.mostRecentWeight()) + (heightFactor *
                (float) Record.height) - (ageFactor * Record.age));
    }

    /**
     * Returns the activity level factor used by the Harris-Benedict equation by using the index
     * in the array present in this fragment's spinner
     *
     * @return The activity level factor used by the Harris-Benedict equation
     */
    private float activityLevelFactor() {
        switch (Record.activityLevel) {
            default:
            case 0: // Little or no exercise
                return 1.2f;
            case 1: // Light exercise 1-3 days/week
                return 1.375f;
            case 2: // Moderate exercise 3-5 days/week
                return 1.55f;
            case 3: // Hard exercise 6-7 days/week
                return 1.725f;
            case 4: // Very hard exercise daily and physical job
                return 1.9f;
        }
    }
}
