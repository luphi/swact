package luphi.swact.views.calorie;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import luphi.swact.R;
import luphi.swact.views.SWaCTFragment;

/**
 * A host fragment containing a bottom navigation view, frame for content, and the logic for
 * switching out any child fragments related to calories
 */
public class CalorieDrawerFragment extends SWaCTFragment {
    /**
     * The tag, used by the fragment manager, of the fragment currently in the content container
     */
    private String mActiveFragment = "calories_overview";

    /**
     * Called by the system to create the view
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.drawer_calories, container, false);
    }

    /**
     * Called by the system following view creation; sets up the fragments to be exchanged by the
     * bottom navigation view
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getView() != null) {
            // Add the initial fragment (overview) into the content container
            final FragmentManager fragmentManager = getChildFragmentManager();
            getChildFragmentManager().beginTransaction().replace(R.id.calories_content_container,
                    new CalorieOverviewFragment(), "calories_overview").commit();
            // Instruct the bottom navigation view on which fragment to load for each item
            ((BottomNavigationView) getView().findViewById(R.id.calories_bottom_nav_view)).
                    setOnNavigationItemSelectedListener(
                            new BottomNavigationView.OnNavigationItemSelectedListener() {
                                @Override
                                public boolean onNavigationItemSelected(
                                        @NonNull MenuItem menuItem) {
                                    // Determine which fragment should be inserted into the content
                                    // container by choosing the appropriate fragment tag
                                    String fragmentTagToInsert;
                                    switch (menuItem.getItemId()) {
                                        case R.id.nav_calories_overview:
                                            fragmentTagToInsert = "calories_overview";
                                            break;
                                        case R.id.nav_calories_history:
                                            fragmentTagToInsert = "calories_history";
                                            break;
                                        case R.id.nav_calories_tdee:
                                            fragmentTagToInsert = "calories_tdee";
                                            break;
                                        default:
                                            return false;
                                    }
                                    // Retrieve the fragment by tag from the fragment manager if it
                                    // has previously been inserted
                                    Fragment fragment =
                                            fragmentManager.findFragmentByTag(fragmentTagToInsert);
                                    // If it was previously created and inserted
                                    if (fragment != null)
                                        fragmentManager.beginTransaction().replace(
                                                R.id.calories_content_container,
                                                fragment).commit();
                                        // If the fragment must be created before being inserted
                                    else {
                                        switch (fragmentTagToInsert) {
                                            case "calories_overview":
                                                fragment = new CalorieOverviewFragment();
                                                break;
                                            case "calories_history":
                                                fragment = new CalorieHistoryFragment();
                                                break;
                                            case "calories_tdee":
                                                fragment = new CalorieTdeeFragment();
                                                break;
                                            default:
                                                return false;
                                        }
                                        fragmentManager.beginTransaction().replace(
                                                R.id.calories_content_container, fragment,
                                                fragmentTagToInsert).commit();
                                    }
                                    mActiveFragment = fragmentTagToInsert;
                                    return true; // Indicates the navigation item should be selected
                                }
                            });
        }
    }

    /**
     * Called whenever this fragment is shown; refreshes the active fragment
     */
    @Override
    public void refresh() {
        Fragment fragment = getChildFragmentManager().findFragmentByTag(mActiveFragment);
        if (fragment != null)
            ((SWaCTFragment) fragment).refresh();
    }

    /**
     * Called on a back event; defers to the active fragment if it consumes the event or navigates
     * back to the overview fragment if not consumed
     *
     * @return True if this fragment consumed (made use of) the event
     */
    @Override
    public boolean onBackPressed() {
        // If the shown fragment is not overview fragment
        if (!mActiveFragment.equals("calories_overview") && (getView() != null)) {
            // Navigate back to the overview fragment
            ((BottomNavigationView) getView().findViewById(R.id.calories_bottom_nav_view)).
                    setSelectedItemId(R.id.nav_calories_overview);
            return true;
        }
        Fragment fragment = getChildFragmentManager().findFragmentByTag(mActiveFragment);
        if (fragment != null)
            return ((SWaCTFragment) fragment).onBackPressed();
        return false;
    }
}
