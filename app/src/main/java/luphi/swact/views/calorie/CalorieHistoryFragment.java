package luphi.swact.views.calorie;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;

import java.util.LinkedList;
import java.util.List;

import luphi.swact.R;
import luphi.swact.Record;
import luphi.swact.adapters.CaloriesAdapter;
import luphi.swact.models.Calories;
import luphi.swact.views.SWaCTFragment;

/**
 * A fragment hosted in the calories drawer containing a list (history) of calorie recordings
 */
public class CalorieHistoryFragment extends SWaCTFragment {
    /**
     * An adapter for wrapping calories recordings and creating their list item view
     */
    private CaloriesAdapter mCaloriesAdapter = null;

    /**
     * A copy of the last known calorie data, used to determine if the list should be refreshed
     */
    private List<Calories> mLastKnownData = null;

    /**
     * Called by the system to create the view
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.calories_history, container, false);
    }

    /**
     * Called by the system following view creation; attaches the adapter to the list
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getView() != null) {
            ListView listView = getView().findViewById(R.id.calories_history_list);
            mCaloriesAdapter = new CaloriesAdapter(getContext(), mIsNightModeOn);
            listView.setAdapter(mCaloriesAdapter);
        }
    }

    /**
     * Called whenever this fragment is shown; refreshes the list if appropriate
     */
    @Override
    public void refresh() {
        if (getView() != null) {
            View noHistoryText = getView().findViewById(R.id.calories_history_no_history_label);
            ListView historyList = getView().findViewById(R.id.calories_history_list);
            // If there are no list items to display
            if (Record.Calories.isEmpty()) {
                // Display the "no history" notice and hide the list
                noHistoryText.setVisibility(View.VISIBLE);
                historyList.setVisibility(View.GONE);
            } else {
                // Display the list and hide the "no history" notice
                noHistoryText.setVisibility(View.GONE);
                historyList.setVisibility(View.VISIBLE);
            }
            // True if weight records were changed or this fragment is newly created
            boolean isDataModified = (mLastKnownData == null) ||
                    !mLastKnownData.equals(Record.Calories);
            // If a copy of the records should be made
            if (isDataModified)
                mLastKnownData = new LinkedList<>(Record.Calories);
            // If the list should be refreshed
            if (isDataModified || isNewDay()) {
                mCaloriesAdapter = new CaloriesAdapter(getContext(), mIsNightModeOn);
                historyList.setAdapter(mCaloriesAdapter);
            }
        }
    }
}
