package luphi.swact.views;

import android.content.res.Configuration;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.Date;

/**
 * A child of the standard Fragment class which acts as a parent for this app's Fragments, providing
 * helper methods and events knowing when to update views
 */
public class SWaCTFragment extends Fragment {
    /**
     * A date object representing the last date/time the fragment was made visible (i.e. the most
     * recent time the fragment was switched to or most recent time the app was resumed)
     */
    private Date mLastShownDate = new Date();

    /**
     * A flag indicating, if true, that night mode, or dark mode, is enabled at the system level
     */
    protected boolean mIsNightModeOn = false;

    /**
     * Called by the system to create the view; overridden here to determine if night mode is on
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (getContext() != null) {
            switch (getContext().getResources().getConfiguration().uiMode &
                    Configuration.UI_MODE_NIGHT_MASK) {
                case Configuration.UI_MODE_NIGHT_YES:
                    mIsNightModeOn = true;
                    break;
                case Configuration.UI_MODE_NIGHT_NO:
                case Configuration.UI_MODE_NIGHT_UNDEFINED:
                default:
                    mIsNightModeOn = false;
                    break;
            }
        }
        return null;
    }

    /**
     * Called by the system when the fragment is visible to the user and actively running
     */
    @Override
    public void onResume() {
        super.onResume();
        // In particular, this is useful for the first draw of the home fragment after
        // initialization and for cases where a new day has begun since this app was last in focus
        // as some fragments have date-dependent stuff
        refresh();
        // Track the date of the most recent time at which this fragment was made visible
        mLastShownDate = new Date();
    }

    /**
     * Called by the system when the hidden state (hidden or shown) of the fragment has changed;
     * this app's fragments are all added to their appropriate frames and hidden or shown as they
     * are selected with the drawer or bottom navigation views
     */
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        // If the fragment is being shown
        if (!hidden) {
            // Let the fragment decide if anything should be visually updated/refreshed
            refresh();
            // Track the date of the most recent time at which this fragment was made visible
            mLastShownDate = new Date();
        }
    }

    /**
     * Each fragment has its own routine for refreshing/updating its views; this method is
     * overridden by child fragments and tailored to perform those operations
     */
    public void refresh() {
    }

    /**
     * Called by the activity in the event of a back press event.  The return value indicates, if
     * true, that the event has been consumed.  In other words, if the event caused some action.
     * Child fragments override this method if they make use of the event.
     *
     * @return True if the fragment consumed (made use of) the event
     */
    public boolean onBackPressed() {
        return false;
    }

    /**
     * Returns true if a new day has been entered since the last time this fragment was shown
     *
     * @return True if the fragment is being shown on a new day
     */
    protected boolean isNewDay() {
        return !DateUtils.isToday(mLastShownDate.getTime());
    }
}
