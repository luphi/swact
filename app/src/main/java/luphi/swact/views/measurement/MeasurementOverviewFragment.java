package luphi.swact.views.measurement;

import android.animation.Animator;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import luphi.swact.Utils;
import luphi.swact.SWaCTActivity;
import luphi.swact.R;
import luphi.swact.Record;
import luphi.swact.models.Measurement;
import luphi.swact.views.SWaCTFragment;

/**
 * A fragment hosted in the measurements drawer containing a chart, relevant information, and a
 * button for adding new entries
 */
public class MeasurementOverviewFragment extends SWaCTFragment {
    /**
     * A flag indicating, if true, that the floating action button (FAB) menu is currently open
     */
    private boolean mIsFabMenuOpen = false;

    /**
     * The always-visible floating action button (FAB) that opens the submenu with additional FABs
     */
    private FloatingActionButton mAddFab = null;

    /**
     * A layout (view) containing the FAB for adding a measurement entry and its adjacent label
     */
    private View mAddMeasurementContainer = null;

    /**
     * A layout (view) containing the FAB for adding a measurement type and its adjacent label
     */
    private View mAddMeasurementTypeContainer = null;


    /**
     * A layout (view) containing the FAB for removing a measurement type and its adjacent label
     */
    private View mRemoveMeasurementTypeContainer = null;

    /**
     * A view describing the action of the add measurement FAB
     */
    private View mAddMeasurementLabel = null;

    /**
     * A view describing the action of the add type FAB
     */
    private View mAddMeasurementTypeLabel = null;

    /**
     * A view describing the action of the remove type FAB
     */
    private View mRemoveMeasurementTypeLabel = null;

    /**
     * A copy of the last known measurements data, used to determine if the chart should be
     * refreshed
     */
    private List<Measurement> mLastKnownData = null;

    /**
     * Called by the system to create the view
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.measurements_overview, container, false);
    }

    /**
     * Called by the system following view creation; sets up the chart's styling and creates
     * callbacks for the add buttons
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getView() != null) {
            LineChart lineChart = getView().findViewById(R.id.measurements_overview_chart);
            lineChart.setNoDataTextColor(getResources().getColor(R.color.color_accent));
            // Disable multiple things that are unwanted (background grid, individual value
            // selection, the chart description, and the right-hand-side Y axis)
            lineChart.setDrawGridBackground(false);
            lineChart.setHighlightPerDragEnabled(false);
            lineChart.setHighlightPerTapEnabled(false);
            lineChart.getDescription().setEnabled(false);
            lineChart.getAxisRight().setEnabled(false);
            // Restrict the Y axis to values in the visible X range.  The Y axis will scale when the
            // user adjusts the X axis.  Zooming in on the Y axis is still possible.
            lineChart.setAutoScaleMinMaxEnabled(true);
            // Disable the Y axis grid
            lineChart.getAxisLeft().setDrawGridLines(false);
            // Change the legend color forms to circle and move the legend to the top-top corner
            Legend legend = lineChart.getLegend();
            legend.setEnabled(true);
            legend.setForm(Legend.LegendForm.CIRCLE);
            legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
            legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
            XAxis xAxis = lineChart.getXAxis();
            // Move the X axis to the bottom and disable its grid
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setDrawAxisLine(false);
            xAxis.setDrawGridLines(false);
            xAxis.setValueFormatter(new ValueFormatter() {
                private DateFormat mFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());
                @Override
                public String getFormattedValue(float value) {
                    // Entry X values are dates in the format of a float for which their values are
                    // the count of milliseconds since the Unix epoch.  The format used here returns
                    // a string akin to "20 Nov" indicating the day and month.
                    return mFormat.format(new Date((long) value));
                }
            });
            mAddFab = getView().findViewById(R.id.measurements_overview_add_button);
            FloatingActionButton addMeasurementFab =
                    getView().findViewById(R.id.measurements_overview_add_measurement_button),
                    addMeasurementTypeFab = getView().findViewById(
                            R.id.measurements_overview_add_measurement_type_button),
                    removeMeasurementTypeFab = getView().findViewById(
                            R.id.measurements_overview_remove_measurement_type_button);
            mAddFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mIsFabMenuOpen)
                        closeAddSubmenu();
                    else
                        openAddSubmenu();
                }
            });
            addMeasurementFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getActivity() != null)
                        ((SWaCTActivity) getActivity()).addMeasurement();
                    closeAddSubmenu();
                }
            });
            addMeasurementTypeFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getActivity() != null)
                        ((SWaCTActivity) getActivity()).addMeasurementsType();
                    closeAddSubmenu();
                }
            });
            removeMeasurementTypeFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getActivity() != null)
                        ((SWaCTActivity) getActivity()).removeMeasurementsType();
                    closeAddSubmenu();
                }
            });
            mAddMeasurementContainer = getView().findViewById(
                    R.id.measurements_overview_add_measurement_container);
            mAddMeasurementTypeContainer = getView().findViewById(
                    R.id.measurements_overview_add_measurement_type_container);
            mRemoveMeasurementTypeContainer = getView().findViewById(
                    R.id.measurements_overview_remove_measurement_type_container);
            mAddMeasurementLabel = getView().findViewById(
                    R.id.measurements_overview_add_measurement_cardview);
            mAddMeasurementTypeLabel = getView().findViewById(
                    R.id.measurements_overview_add_measurement_type_cardview);
            mRemoveMeasurementTypeLabel = getView().findViewById(
                    R.id.measurements_overview_remove_measurement_type_cardview);
            lineChart.invalidate(); // Refresh the chart
        }
    }

    /**
     * Called whenever this fragment is shown; refreshes the chart by generating entry lists and
     * applying styling to them
     */
    @Override
    public void refresh() {
        if (getView() != null) {
            // If the add sub menu was left open when the user navigated away, or left the app
            if (mIsFabMenuOpen)
                closeAddSubmenuImmediate(); // Close it without delay
            LineChart lineChart = getView().findViewById(R.id.measurements_overview_chart);
            if ((mLastKnownData == null) || !mLastKnownData.equals(Record.Measurements)) {
                mLastKnownData = new LinkedList<>(Record.Measurements);
                if (Record.Measurements.size() > 0) {
                    int colorIndex = 0, maxEntriesPerTypeThisWeek = 0;
                    LineData lineData = new LineData();
                    for (String measurementType : Record.MeasurementsTypes) {
                        List<Entry> entries = new LinkedList<>();
                        int entriesThisWeek = 0;
                        for (Measurement measurement : Record.Measurements) {
                            if (measurement.type.equals(measurementType)) {
                                if (Utils.daysAgo(measurement.date) < 7)
                                    entriesThisWeek += 1;
                                entries.add(new Entry(measurement.date.getTime(),
                                        measurement.value));
                            }
                        }
                        if (entriesThisWeek > maxEntriesPerTypeThisWeek)
                            maxEntriesPerTypeThisWeek = entriesThisWeek;
                        // Sort the entries by date, in ascending order
                        Collections.reverse(entries);
                        LineDataSet lineDataSet = new LineDataSet(entries, measurementType);
                        // Apply some styling to the data (line width 3, line color, circle [value]
                        // color, circle size[s], and enable the value labels)
                        lineDataSet.setLineWidth(4f);
                        lineDataSet.setColor(ColorTemplate.PASTEL_COLORS[colorIndex]);
                        lineDataSet.setCircleColor(ColorTemplate.PASTEL_COLORS[colorIndex]);
                        lineDataSet.setCircleRadius(4f);
                        lineDataSet.setCircleHoleRadius(3f);
                        lineDataSet.setDrawValues(true);
                        if (mIsNightModeOn)
                            lineDataSet.setValueTextColor(Color.WHITE);
                        else
                            lineDataSet.setValueTextColor(Color.BLACK);
                        lineData.addDataSet(lineDataSet);
                        // Select the next color to use in preparation for the next dataset
                        if (colorIndex == (ColorTemplate.PASTEL_COLORS.length - 1))
                            colorIndex = 0;
                        else
                            colorIndex += 1;
                    }
                    lineChart.setData(lineData);
                    // Set the visible X range maximum to the middle of the current day
                    lineChart.getXAxis().setAxisMaximum((float)
                            Utils.middleOfDay(new Date()).getTime());
                    // If the oldest entry is less than 7 days (a week) ago
                    if (Utils.daysAgo(Record.Measurements.get(Record.Measurements.size() - 1).date)
                            < 7) {
                        // Disable user input because there's really no point with so little data
                        lineChart.setTouchEnabled(false);
                        // Limit the number of X axis labels to match the number of days and limit
                        // the visible X range minimum to the width of one day (86400000 ms)
                        lineChart.getXAxis().setLabelCount(maxEntriesPerTypeThisWeek, true);
                        lineChart.setVisibleXRangeMinimum(86400000f);
                        // With input being disabled, autoscaling isn't needed
                        lineChart.setAutoScaleMinMaxEnabled(false);
                    } else {
                        //Enable user input so he/she can manipulate the viewport (visible data)
                        lineChart.setTouchEnabled(true);
                        // Limit the number of X axis labels a week's worth of days and limit the
                        // visible X range minimum to the width of seven points (86400000 * 6 ms)
                        lineChart.getXAxis().setLabelCount(7, true);
                        lineChart.setVisibleXRangeMinimum(86400000f * 6f);
                        // Restrict the Y axis to values in the visible X range.  The Y axis will
                        // scale when the user adjusts the X axis.  Zooming in on the Y axis is
                        // still possible.
                        lineChart.setAutoScaleMinMaxEnabled(true);
                    }
                    // If the oldest entry is more than 30 days (~a month) ago
                    if (Utils.daysAgo(Record.Weights.get(Record.Weights.size() - 1).date) > 30) {
                        // Zoom in an excessive amount and allow the chart to fit itself to 30 days
                        // worth of entries, then return the minimum range to seven days worth
                        lineChart.setVisibleXRangeMinimum(86400000f * 29f);
                        lineChart.zoom(1000f, 1f, 0f, 0f);
                        lineChart.setVisibleXRangeMinimum(86400000f * 6f);
                        // moveViewToX() is intended to set the left edge of the viewport but, to
                        // make our lives easier, we'll set it to the right-most data point and let
                        // the chart fit itself
                        lineChart.moveViewToX(new Date().getTime());
                    }
                }
                else
                    lineChart.setData(null);
                if (mIsNightModeOn) {
                    lineChart.setNoDataTextColor(Color.WHITE);
                    lineChart.getLegend().setTextColor(Color.WHITE);
                    lineChart.getXAxis().setTextColor(Color.WHITE);
                    lineChart.getAxisLeft().setTextColor(Color.WHITE);
                }
                else {
                    lineChart.setNoDataTextColor(Color.BLACK);
                    lineChart.getLegend().setTextColor(Color.BLACK);
                    lineChart.getXAxis().setTextColor(Color.BLACK);
                    lineChart.getAxisLeft().setTextColor(Color.BLACK);
                }
                lineChart.invalidate();
            }
        }
    }

    /**
     * Called on a back event; closes the add sebmenu if it's open
     *
     * @return True if this fragment consumed (made use of) the event
     */
    @Override
    public boolean onBackPressed() {
        if (mIsFabMenuOpen) {
            closeAddSubmenu();
            return true;
        }
        return false;
    }

    /**
     * Opens the add sebmenu by changing the visibility of the layouts, applies two animations
     * (translation on the Y axis and alpha [invisible to visible]), and animates the always-visible
     * add button by rotating it 225 degrees causing it appear to be a close "X" icon
     */
    private void openAddSubmenu() {
        mIsFabMenuOpen = true;
        mAddMeasurementContainer.setVisibility(View.VISIBLE);
        mAddMeasurementTypeContainer.setVisibility(View.VISIBLE);
        mRemoveMeasurementTypeContainer.setVisibility(View.VISIBLE);
        mAddFab.animate().rotation(225);
        mAddMeasurementContainer.animate().translationY(-getResources().getDimension(
                R.dimen.fab_margin_child1));
        mAddMeasurementTypeContainer.animate().translationY(-getResources().getDimension(
                R.dimen.fab_margin_child2));
        mRemoveMeasurementTypeContainer.animate().translationY(-getResources().getDimension(
                R.dimen.fab_margin_child3));
        mAddMeasurementLabel.animate().alpha(1f);
        mAddMeasurementTypeLabel.animate().alpha(1f);
        mRemoveMeasurementTypeLabel.animate().alpha(1f);
    }

    /**
     * Closes the add submenu by changing the visibility of the layouts, applies to animations
     * (translation on the Y axis and alpha [visible to invisible]), and animates the always-visible
     * add button by rotating it -225 degrees causing it to appear as its original "+" icon
     */
    private void closeAddSubmenu() {
        mIsFabMenuOpen = false;
        mAddFab.animate().rotation(0);
        mAddMeasurementContainer.animate().translationY(0);
        mAddMeasurementTypeContainer.animate().translationY(0);
        mRemoveMeasurementTypeContainer.animate().translationY(0);
        mAddMeasurementLabel.animate().alpha(0f);
        mAddMeasurementTypeLabel.animate().alpha(0f);
        mRemoveMeasurementTypeLabel.animate().alpha(0f).setListener(
                new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        if (!mIsFabMenuOpen) {
                            mAddMeasurementContainer.setVisibility(View.GONE);
                            mAddMeasurementTypeContainer.setVisibility(View.GONE);
                            mRemoveMeasurementTypeContainer.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                });
    }

    /**
     * Closes the add submenu by changing visibilities and rotations without delay
     */
    private void closeAddSubmenuImmediate() {
        mIsFabMenuOpen = false;
        mAddFab.setRotation(0);
        mAddMeasurementContainer.setY(0);
        mAddMeasurementTypeContainer.setY(0);
        mRemoveMeasurementTypeContainer.setY(0);
        mAddMeasurementLabel.setAlpha(0f);
        mAddMeasurementTypeLabel.setAlpha(0f);
        mRemoveMeasurementTypeLabel.setAlpha(0f);
        mAddMeasurementContainer.setVisibility(View.GONE);
        mAddMeasurementTypeContainer.setVisibility(View.GONE);
        mRemoveMeasurementTypeContainer.setVisibility(View.GONE);
    }
}
