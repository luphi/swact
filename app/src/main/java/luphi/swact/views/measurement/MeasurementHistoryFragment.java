package luphi.swact.views.measurement;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;

import java.util.LinkedList;
import java.util.List;

import luphi.swact.R;
import luphi.swact.Record;
import luphi.swact.adapters.MeasurementsAdapter;
import luphi.swact.models.Measurement;
import luphi.swact.views.SWaCTFragment;

/**
 * A fragment hosted in the measurements drawer containing a list (history) of measurements
 * recordings
 */
public class MeasurementHistoryFragment extends SWaCTFragment {
    /**
     * An adapter for wrapping measurements recordings and creating their list item view
     */
    private MeasurementsAdapter mMeasurementsAdapter = null;

    /**
     * A copy of the last known measurements data, used to determine if the list should be refreshed
     */
    private List<Measurement> mLastKnownData = null;

    /**
     * Called by the system to create the view
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.measurements_history, container, false);
    }

    /**
     * Called by the system following view creation; attaches the adapter to the list
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getView() != null) {
            ListView listView = getView().findViewById(R.id.measurements_history_list);
            mMeasurementsAdapter = new MeasurementsAdapter(getContext(), mIsNightModeOn);
            listView.setAdapter(mMeasurementsAdapter);
        }
    }

    /**
     * Called whenever this fragment is shown; refreshes the list if appropriate
     */
    @Override
    public void refresh() {
        if (getView() != null) {
            View noHistoryText = getView().findViewById(R.id.measurements_history_no_history_label);
            ListView historyList = getView().findViewById(R.id.measurements_history_list);
            // If there are no list items to display
            if (Record.Measurements.isEmpty()) {
                // Display the "no history" notice and hide the list
                noHistoryText.setVisibility(View.VISIBLE);
                historyList.setVisibility(View.GONE);
            } else {
                // Display the list and hide the "no history" notice
                noHistoryText.setVisibility(View.GONE);
                historyList.setVisibility(View.VISIBLE);
            }
            // True if measurement records were changed or this fragment is newly created
            boolean isDataModified = (mLastKnownData == null) ||
                    !mLastKnownData.equals(Record.Measurements);
            // If a copy of the records should be made
            if (isDataModified)
                mLastKnownData = new LinkedList<>(Record.Measurements);
            // If the list should be refreshed
            if (isDataModified || isNewDay()) {
                mMeasurementsAdapter = new MeasurementsAdapter(getContext(), mIsNightModeOn);
                historyList.setAdapter(mMeasurementsAdapter);
            }
        }
    }
}
