package luphi.swact.views.home;

import android.animation.Animator;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import luphi.swact.Utils;
import luphi.swact.SWaCTActivity;
import luphi.swact.R;
import luphi.swact.Record;
import luphi.swact.models.Calories;
import luphi.swact.models.Weight;
import luphi.swact.views.SWaCTFragment;

/**
 * A fragment acting as this app's home screen, displaying a couple charts, a projection, rates, and
 * buttons for adding all three types of entries
 */
public class HomeFragment extends SWaCTFragment {
    /**
     * A flag indicating, if true, that the floating action button (FAB) menu is currently open
     */
    private boolean mIsFabMenuOpen = false;

    /**
     * The always-visible floating action button (FAB) that opens the submenu with additional FABs
     */
    private FloatingActionButton mAddFab = null;

    /**
     * A layout (view) containing the FAB for adding a weight entry and its adjacent label
     */
    private View mAddWeightLayout = null;

    /**
     * A layout (view) containing the FAB for adding a calorie entry and its adjacent label
     */
    private View mAddCaloriesLayout = null;

    /**
     * A layout (view) containing the FAB for adding a measurement entry and its adjacent label
     */
    private View mAddMeasurementLayout = null;

    /**
     * A view describing the action of the weight FAB
     */
    private View mAddWeightLabel = null;

    /**
     * A view describing the action of the calorie FAB
     */
    private View mAddCaloriesLabel = null;

    /**
     * A view describing the action of the measurement FAB
     */
    private View mAddMeasurementLabel = null;

    /**
     * A copy of the last known calorie data, used to determine if the weekly calorie chart should
     * be refreshed
     */
    private List<Calories> mLastKnownCaloriesData = null;

    /**
     * A copy of the last known weight data, used to determine if the weekly weight chart should be
     * refreshed
     */
    private List<Weight> mLastKnownWeightData = null;

    /**
     * A string copy of the last known "use imperial weight" flag (referring to weight units); in
     * the form of a string so that it may hold three values: null, "true," or "false"
     */
    private String mLastKnownUseImperialWeight = null;

    /**
     * Called by the system to create the view
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.home, container, false);
    }

    /**
     * Called by the system following view creation; adds callbacks to floating action buttons and
     * sets up styling for the charts
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getView() != null) {
            mAddFab = getView().findViewById(R.id.home_add_button);
            FloatingActionButton addWeightFab = getView().findViewById(R.id.home_add_weight_button),
                    addCaloriesFab = getView().findViewById(R.id.home_add_calories_button),
                    addMeasurementFab = getView().findViewById(R.id.home_add_measurement_button);
            mAddWeightLayout = getView().findViewById(R.id.home_add_weight_container);
            mAddCaloriesLayout = getView().findViewById(R.id.home_add_calories_container);
            mAddMeasurementLayout = getView().findViewById(R.id.home_add_measurement_container);
            mAddWeightLabel = getView().findViewById(R.id.home_add_weight_cardview);
            mAddCaloriesLabel = getView().findViewById(R.id.home_add_calories_cardview);
            mAddMeasurementLabel = getView().findViewById(R.id.home_add_measurement_cardview);
            LineChart weightChart = getView().findViewById(R.id.home_weight_chart);
            BarChart caloriesChart = getView().findViewById(R.id.home_calories_chart);
            mAddFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mIsFabMenuOpen)
                        closeAddSubmenu();
                    else
                        openAddSubmenu();
                }
            });
            addWeightFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getActivity() != null)
                        ((SWaCTActivity) getActivity()).addWeight();
                    closeAddSubmenu();
                }
            });
            addCaloriesFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getActivity() != null)
                        ((SWaCTActivity) getActivity()).addCalories();
                    closeAddSubmenu();
                }
            });
            addMeasurementFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getActivity() != null)
                        ((SWaCTActivity) getActivity()).addMeasurement();
                    closeAddSubmenu();
                }
            });
            // Disable multiple things that are unwanted (background grid, individual value
            // selection, the chart description, the right-hand-side Y axis, and input)
            weightChart.setDrawGridBackground(false);
            weightChart.setHighlightPerDragEnabled(false);
            weightChart.setHighlightPerTapEnabled(false);
            weightChart.getDescription().setEnabled(false);
            weightChart.getAxisRight().setEnabled(false);
            weightChart.setTouchEnabled(false);
            // Position the legend (top right)
            Legend legend = weightChart.getLegend();
            legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
            legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
            // Disable the Y axis grid
            weightChart.getAxisLeft().setDrawGridLines(false);
            XAxis xAxis = weightChart.getXAxis();
            // Move the X axis to the bottom and disable its grid
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setDrawAxisLine(false);
            xAxis.setDrawGridLines(false);
            xAxis.setValueFormatter(new ValueFormatter() {
                private DateFormat mFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());

                @Override
                public String getFormattedValue(float value) {
                    // Entry X values are dates in the format of a float for which their values are
                    // the count of milliseconds since the Unix epoch.  The format used here returns
                    // a string akin to "20 Nov" indicating the day and month.
                    return mFormat.format(new Date((long) value));
                }
            });
            caloriesChart.setNoDataTextColor(getResources().getColor(R.color.color_accent));
            // Disable multiple things that are unwanted (background grid, individual value
            // selection, the chart description, the right-hand-side Y axis, and input)
            caloriesChart.setDrawGridBackground(false);
            caloriesChart.setHighlightPerDragEnabled(false);
            caloriesChart.setHighlightPerTapEnabled(false);
            caloriesChart.getDescription().setEnabled(false);
            caloriesChart.getAxisRight().setEnabled(false);
            caloriesChart.setTouchEnabled(false);
            caloriesChart.setFitBars(true);
            // Position the legend (top right)
            legend = caloriesChart.getLegend();
            legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
            legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
            // Disable the Y axis grid
            caloriesChart.getAxisLeft().setDrawGridLines(false);
            xAxis = caloriesChart.getXAxis();
            // Move the X axis to the bottom and disable its grid
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setDrawAxisLine(false);
            xAxis.setDrawGridLines(false);
            xAxis.setValueFormatter(new ValueFormatter() {
                private DateFormat mFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());

                @Override
                public String getFormattedValue(float value) {
                    // Entry X values are dates in the format of a float for which their values are
                    // the count of milliseconds since the Unix epoch.  The format used here returns
                    // a string akin to "20 Nov" indicating the day and month.
                    return mFormat.format(new Date((long) value));
                }
            });
        }
    }

    /**
     * Called whenever this fragment is shown; refreshes the charts by generating entry lists and
     * applying styling to them, and fills out the daily values, rates, and, after calculating it,
     * projection views
     */
    @Override
    public void refresh() {
        if (getView() != null) {
            // If the add sub menu was left open when the user navigated away, or left the app
            if (mIsFabMenuOpen)
                closeAddSubmenuImmediate(); // Close it without delay
            TextView todaysWeightText = getView().findViewById(R.id.home_todays_weight),
                    todaysWeightUnitsText = getView().findViewById(R.id.home_todays_weight_units),
                    todaysCaloriesText = getView().findViewById(R.id.home_todays_calories),
                    weeklyWeightRateText = getView().findViewById(R.id.home_weight_per_week),
                    weeklyWeightRateUnitsText =
                            getView().findViewById(R.id.home_weight_per_week_units),
                    dailyCalorieRateText = getView().findViewById(R.id.home_calories_per_day),
                    daysRemainingGoalText = getView().findViewById(R.id.home_days_remaining_goal),
                    daysRemainingText = getView().findViewById(R.id.home_days_remaining),
                    caloriesRemainingText = getView().findViewById(R.id.home_calories_remaining),
                    caloriesRemainingGoalText =
                            getView().findViewById(R.id.home_calories_remaining_goal);
            LineChart weightChart = getView().findViewById(R.id.home_weight_chart);
            BarChart caloriesChart = getView().findViewById(R.id.home_calories_chart);
            // Create a list of weight recordings this week, use by multiple sections below
            List<Weight> weightsThisWeek = new LinkedList<>();
            for (Weight weight : Record.Weights) {
                // If this weight record is more than a week ago
                if (Utils.daysAgo(weight.date) > 6)
                    break; // There's no more (older) data to add
                weightsThisWeek.add(new Weight(weight.date, weight.weight, null));
            }
            // True if weight records were changed or this fragment is newly created
            boolean isDataModified = (mLastKnownWeightData == null) ||
                    (!mLastKnownWeightData.equals(Record.Weights)),
                    // True if the preferred weight units (pounds or kilograms) has changed
                    areUnitsModified = (mLastKnownUseImperialWeight == null) ||
                            !mLastKnownUseImperialWeight.equals(String.valueOf(
                                    Record.useImperialWeight));
            if (isDataModified || areUnitsModified || isNewDay()) {
                // If a copy of the records should be made
                if (isDataModified)
                    mLastKnownWeightData = new LinkedList<>(Record.Weights);
                // If a copy of the preferred units should be made
                if (areUnitsModified)
                    mLastKnownUseImperialWeight = String.valueOf(Record.useImperialWeight);
                if (Record.Weights.size() > 0) {
                    // Create a list of chart entries from the list of weights
                    List<Entry> entries = new LinkedList<>();
                    for (Weight weight : weightsThisWeek) {
                        if (Record.useImperialWeight)
                            entries.add(new Entry(weight.date.getTime(),
                                    Utils.kilogramsToPounds(weight.weight)));
                        else
                            entries.add(new Entry(weight.date.getTime(), weight.weight));
                    }
                    XAxis xAxis = weightChart.getXAxis();
                    // Set the number X axis labels to 7 (days)
                    xAxis.setLabelCount(7, true);
                    // Set the visible X range, from midday a week ago to midday today
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(Utils.middleOfDay(new Date()));
                    calendar.add(Calendar.DATE, -6);
                    xAxis.setAxisMinimum((float) calendar.getTimeInMillis());
                    xAxis.setAxisMaximum((float) Utils.middleOfDay(new Date()).getTime());
                    // Sort the entries by date, in ascending order
                    Collections.reverse(entries);
                    LineDataSet lineDataSet = new LineDataSet(entries, "Weight");
                    // Apply some styling to the data (line width 3, line color, circle [value]
                    // color, circle size[s], and disable the value labels)
                    lineDataSet.setLineWidth(4f);
                    lineDataSet.setColor(getResources().getColor(R.color.color_accent));
                    lineDataSet.setCircleColor(getResources().getColor(R.color.color_accent));
                    lineDataSet.setCircleRadius(4f);
                    lineDataSet.setCircleHoleRadius(3f);
                    if (mIsNightModeOn)
                        lineDataSet.setValueTextColor(Color.WHITE);
                    else
                        lineDataSet.setValueTextColor(Color.BLACK);
                    weightChart.setData(new LineData(lineDataSet));
                    weightChart.moveViewToX(new Date().getTime());
                } else
                    weightChart.setData(null);
                if (mIsNightModeOn) {
                    weightChart.setNoDataTextColor(Color.WHITE);
                    weightChart.getLegend().setTextColor(Color.WHITE);
                    weightChart.getXAxis().setTextColor(Color.WHITE);
                    weightChart.getAxisLeft().setTextColor(Color.WHITE);
                }
                else {
                    weightChart.setNoDataTextColor(Color.BLACK);
                    weightChart.getLegend().setTextColor(Color.BLACK);
                    weightChart.getXAxis().setTextColor(Color.BLACK);
                    weightChart.getAxisLeft().setTextColor(Color.BLACK);
                }
                weightChart.invalidate();
            }
            // Fill out the unit labels for today's weight and the weekly rate
            if (Record.useImperialWeight) {
                todaysWeightUnitsText.setText(R.string.imperial_weight_units);
                weeklyWeightRateUnitsText.setText(getResources().getString(
                        R.string.imperial_weight_units_per_week));
            }
            else {
                todaysWeightUnitsText.setText(R.string.metric_weight_units);
                weeklyWeightRateUnitsText.setText(getResources().getString(
                        R.string.metric_weight_units_per_week));
            }
            // Calculate the change in weight for the last week
            float weeklyWeightRate = trendLineSlope(weightsThisWeek);
            // Create a list of one-per-day calorie recordings from this week, used by multiple
            // sections below
            List<Calories> caloriesThisWeek = new LinkedList<>();
            if (!Record.Calories.isEmpty()) {
                int dailyCalories = 0; // Sum of calories consumed in the same day
                Date dayDate = Record.Calories.get(0).date; // Date object for the working day
                for (Calories calories : Record.Calories) {
                    // If this calories record is more than a week ago
                    if (Utils.daysAgo(calories.date) > 6)
                        break; // There's no more (older) data to add
                    if (!Utils.isSameDay(dayDate, calories.date)) {
                        caloriesThisWeek.add(new Calories(Utils.middleOfDay(dayDate),
                                dailyCalories, null));
                        dailyCalories = 0; // Reset the counter
                        dayDate = calories.date; // The new day to track
                    }
                    dailyCalories += calories.calories;
                }
                // Add the final entry.  Adding entries to the list is done in the above loop
                // when a new day is found.  For the final summation, there will be no new day.
                caloriesThisWeek.add(new Calories(Utils.middleOfDay(dayDate), dailyCalories,
                        null));
            }
            // If there was a change to the recorded calories (added or removed)
            if ((mLastKnownCaloriesData == null) ||
                    !mLastKnownCaloriesData.equals(Record.Calories) || isNewDay()) {
                mLastKnownCaloriesData = new LinkedList<>(Record.Calories);
                if (Record.Calories.size() > 0) {
                    List<BarEntry> entries = new LinkedList<>();
                    // Create a list of chart entries from the list of calories
                    for (Calories calories : caloriesThisWeek)
                        entries.add(new BarEntry(calories.date.getTime(), calories.calories));
                    XAxis xAxis = caloriesChart.getXAxis();
                    // Set the number X axis labels to 7 (days)
                    xAxis.setLabelCount(7, true);
                    // Set the visible X range, from the start of the day a week ago to the end of
                    // the current day
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(Utils.startOfDay(new Date()));
                    calendar.add(Calendar.DATE, -6);
                    xAxis.setAxisMinimum((float) calendar.getTimeInMillis());
                    calendar.setTime(Utils.startOfDay(new Date()));
                    calendar.add(Calendar.DATE, 1);
                    xAxis.setAxisMaximum((float) calendar.getTimeInMillis());
                    // Sort the entries by date, in ascending order
                    Collections.reverse(entries);
                    BarDataSet barDataSet = new BarDataSet(entries, "Calories");
                    barDataSet.setColor(getResources().getColor(R.color.color_accent));
                    if (mIsNightModeOn)
                        barDataSet.setValueTextColor(Color.WHITE);
                    else
                        barDataSet.setValueTextColor(Color.BLACK);
                    BarData barData = new BarData(barDataSet);
                    // Due to using millisecond X values, there is a large (86400000f ms) difference
                    // between each data point and the charts calculate bar width relative to that
                    // difference.  If not set manually, the bar width will be too small to see.
                    barData.setBarWidth(86400000f * 0.9f);
                    caloriesChart.setData(barData);
                } else
                    caloriesChart.setData(null);
                if (mIsNightModeOn) {
                    caloriesChart.setNoDataTextColor(Color.WHITE);
                    caloriesChart.getLegend().setTextColor(Color.WHITE);
                    caloriesChart.getXAxis().setTextColor(Color.WHITE);
                    caloriesChart.getAxisLeft().setTextColor(Color.WHITE);
                }
                else {
                    caloriesChart.setNoDataTextColor(Color.BLACK);
                    caloriesChart.getLegend().setTextColor(Color.BLACK);
                    caloriesChart.getXAxis().setTextColor(Color.BLACK);
                    caloriesChart.getAxisLeft().setTextColor(Color.BLACK);
                }
                caloriesChart.invalidate();
            }
            float todaysWeight = Record.todaysWeight();
            if (Record.useImperialWeight)
                todaysWeight = Utils.kilogramsToPounds(todaysWeight);
            // If a weight has been recorded today
            if (todaysWeight > 0f)
                todaysWeightText.setText(String.format(Locale.getDefault(), "%.1f", todaysWeight));
            else
                todaysWeightText.setText("?");
            int todaysCalories = Record.todaysCalories();
            todaysCaloriesText.setText(String.valueOf(todaysCalories));
            // If the weekly weight change rate doesn't exist (i.e. there are no weight records)
            if (weeklyWeightRate == Float.POSITIVE_INFINITY)
                weeklyWeightRateText.setText("?");
                // If the rate is positive
            else if (weeklyWeightRate > 0f) {
                // Prefix the string with a +
                String formattedWeeklyWeightRate = "+" + String.format(Locale.getDefault(), "%.2f",
                        weeklyWeightRate);
                weeklyWeightRateText.setText(formattedWeeklyWeightRate);
            }
            // If the weekly weight change rate exists
            else
                weeklyWeightRateText.setText(String.format(Locale.getDefault(), "%.2f",
                        weeklyWeightRate));
            // If the average daily caloric consumption can be calculated for this week
            if (!caloriesThisWeek.isEmpty()) {
                int sum = 0;
                for (Calories calories : caloriesThisWeek)
                    sum += calories.calories;
                dailyCalorieRateText.setText(String.format(Locale.getDefault(), "%.0f",
                        (float) sum / (float) caloriesThisWeek.size()));
            }
            else
                dailyCalorieRateText.setText("?");
            float goalWeight = Record.goalWeight;
            float closeEnough = 0.5f; // 0.5 kilograms is close enough to the goal
            if (Record.useImperialWeight) {
                closeEnough = Utils.kilogramsToPounds(closeEnough); // ~1.1 pounds
                goalWeight = Utils.kilogramsToPounds(goalWeight);
            }
            // Set or hide the e.g. "to 155.0 lbs" label
            if (Record.goalWeight != 0f) {
                String formattedDaysRemainingGoalText = "to " +
                        String.format(Locale.getDefault(), "%.1f", goalWeight) + " ";
                if (Record.useImperialWeight)
                    formattedDaysRemainingGoalText +=
                            getResources().getString(R.string.imperial_weight_units);
                else
                    formattedDaysRemainingGoalText +=
                            getResources().getString(R.string.metric_weight_units);
                daysRemainingGoalText.setText(formattedDaysRemainingGoalText);
                daysRemainingGoalText.setVisibility(View.VISIBLE);
            }
            else
                daysRemainingGoalText.setVisibility(View.GONE);
            float daysRemaining, mostRecentWeight = Record.mostRecentWeight(),
                    dailyWeightRate = weeklyWeightRate / 7f;
            if (Record.useImperialWeight)
                mostRecentWeight = Utils.kilogramsToPounds(mostRecentWeight);
            // If there is no goal weight or recorded weight (yet)
            if ((Record.goalWeight == 0) || Record.Weights.isEmpty())
                daysRemaining = Float.NEGATIVE_INFINITY;
                // If the goal weight has been reached, give or take
            else if ((mostRecentWeight > 0f) &&
                    (Math.abs(goalWeight - mostRecentWeight) <= closeEnough))
                daysRemaining = 0f;
                // If there is no progress being made or weight is progressing in the wrong direction
            else if ((dailyWeightRate == 0f) ||
                    (dailyWeightRate * (goalWeight - mostRecentWeight) < 0f))
                daysRemaining = Float.POSITIVE_INFINITY;
                // If the number of remaining days can be calculated
            else
                daysRemaining = (goalWeight - mostRecentWeight) / dailyWeightRate;
            // If there is no goal weight (yet)
            if (daysRemaining == Float.NEGATIVE_INFINITY)
                daysRemainingText.setText("?");
                // If there's an infinite number of days remaining
            else if (daysRemaining == Float.POSITIVE_INFINITY)
                daysRemainingText.setText("∞");
                // If an actual number of remaining days exists
            else {
                // If the number of remaining days came out approximately equal to a solid number
                // (e.g. 38.00008 days)
                if (Utils.fuzzyEquals(daysRemaining, (float) Math.floor(daysRemaining)))
                    daysRemainingText.setText(String.format(Locale.getDefault(), "%.0f",
                            Math.floor(daysRemaining)));
                else
                    daysRemainingText.setText(String.format(Locale.getDefault(), "%.0f",
                            Math.ceil(daysRemaining)));
            }
            // Calculate and set the remaining calories to the daily goal, if said goal exists
            if (Record.goalCalories != 0)
                caloriesRemainingText.setText(String.valueOf(Record.goalCalories - todaysCalories));
            else
                caloriesRemainingText.setText("?");
            // Set or hide the e.g. "to 1800 kcal" label
            if (Record.goalCalories != 0) {
                String formattedCaloriesRemainingGoalText = "to " + Record.goalCalories + " " +
                        getResources().getString(R.string.calories_units);
                caloriesRemainingGoalText.setText(formattedCaloriesRemainingGoalText);
                caloriesRemainingGoalText.setVisibility(View.VISIBLE);
            }
            else
                caloriesRemainingGoalText.setVisibility(View.GONE);
        }
    }

    /**
     * Called on a back event; closes the add sebmenu if it's open
     *
     * @return True if this fragment consumed (made use of) the event
     */
    @Override
    public boolean onBackPressed() {
        if (mIsFabMenuOpen) {
            closeAddSubmenu();
            return true;
        }
        return false;
    }

    /**
     * Opens the add sebmenu by changing the visibility of the layouts, applies two animations
     * (translation on the Y axis and alpha [invisible to visible]), and animates the always-visible
     * add button by rotating it 225 degrees causing it appear to be a close "X" icon
     */
    private void openAddSubmenu() {
        mIsFabMenuOpen = true;
        mAddWeightLayout.setVisibility(View.VISIBLE);
        mAddCaloriesLayout.setVisibility(View.VISIBLE);
        mAddMeasurementLayout.setVisibility(View.VISIBLE);
        mAddFab.animate().rotation(225);
        mAddWeightLayout.animate().translationY(-getResources().getDimension(
                R.dimen.fab_margin_child1));
        mAddCaloriesLayout.animate().translationY(-getResources().getDimension(
                R.dimen.fab_margin_child2));
        mAddMeasurementLayout.animate().translationY(-getResources().getDimension(
                R.dimen.fab_margin_child3));
        mAddWeightLabel.animate().alpha(1f);
        mAddCaloriesLabel.animate().alpha(1f);
        mAddMeasurementLabel.animate().alpha(1f);
    }

    /**
     * Closes the add submenu by changing the visibility of the layouts, applies to animations
     * (translation on the Y axis and alpha [visible to invisible]), and animates the always-visible
     * add button by rotating it -225 degrees causing it to appear as its original "+" icon
     */
    private void closeAddSubmenu() {
        mIsFabMenuOpen = false;
        mAddFab.animate().rotation(0);
        mAddWeightLayout.animate().translationY(0);
        mAddCaloriesLayout.animate().translationY(0);
        mAddMeasurementLayout.animate().translationY(0);
        mAddWeightLabel.animate().alpha(0f);
        mAddCaloriesLabel.animate().alpha(0f);
        mAddMeasurementLabel.animate().alpha(0f).setListener(
                new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        if (!mIsFabMenuOpen) {
                            mAddWeightLayout.setVisibility(View.GONE);
                            mAddCaloriesLayout.setVisibility(View.GONE);
                            mAddMeasurementLayout.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                });
    }

    /**
     * Closes the add submenu by changing visibilities and rotations without delay
     */
    private void closeAddSubmenuImmediate() {
        mIsFabMenuOpen = false;
        mAddFab.setRotation(0);
        mAddWeightLayout.setY(0);
        mAddCaloriesLayout.setY(0);
        mAddMeasurementLayout.setY(0);
        mAddWeightLabel.setAlpha(0f);
        mAddCaloriesLabel.setAlpha(0f);
        mAddMeasurementLabel.setAlpha(0f);
        mAddWeightLayout.setVisibility(View.GONE);
        mAddCaloriesLayout.setVisibility(View.GONE);
        mAddMeasurementLayout.setVisibility(View.GONE);
    }

    /**
     * Calculate the slope (m) of a linear trend line representing the given (date, weight) dataset.
     * The slope's units are kilograms/week.
     *
     * @param weights A list of Weight objects from which to derive the slope
     * @return Linear trend line slope or positive infinity if the slope cannot be calculated
     */
    private float trendLineSlope(List<Weight> weights) {
        if (weights.isEmpty())
            return Float.POSITIVE_INFINITY;
        // The formula used here is
        //     sum((x_i - x_average) * (y_i - y_average)
        // m = ----------------------------------------
        //            sum((x_i - x_average)^2)
        // for which X values are time and Y values are weight.
        // Start by calculating the sums of the X and Y values.
        float xAverage = 0f, yAverage = 0f;
        for (Weight weight : weights) {
            xAverage += weight.date.getTime();
            yAverage += weight.weight;
        }
        // Divide by n to get the average
        xAverage /= weights.size();
        yAverage /= weights.size();
        // Calculate the numerator, sum((x_i - x_average) * (y_i - y_average)
        float numerator = 0f;
        for (Weight weight : weights)
            numerator += (weight.date.getTime() - xAverage) * (weight.weight - yAverage);
        // Calculate the denominator, sum((x_i - x_average)^2)
        float denominator = 0f;
        for (Weight weight : weights)
            denominator += Math.pow(weight.date.getTime() - xAverage, 2);
        // Calculate the slope if possible
        float slope = Float.POSITIVE_INFINITY;
        if (denominator != 0f) {
            slope = numerator / denominator;
            // At this point, the slope's units are kilograms/millisecond but that's a useless unit
            // so let's convert it to the more useful kilograms/week by multiplying by the number of
            // milliseconds in a week
            slope *= 604800000f;
        }
        return slope;
    }
}