package luphi.swact.views.profile;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.NonNull;

import java.util.Locale;

import luphi.swact.R;
import luphi.swact.Record;
import luphi.swact.Utils;
import luphi.swact.views.SWaCTFragment;

/**
 * A fragment that serves the dual purposes of allowing the user to input information that is needed
 * for all features of this app to work and selecting preferences, namely the preferred units
 * (metric or imperial units for height and/or weight)
 */
public class ProfileFragment extends SWaCTFragment {
    /**
     * Foot component of the imperial height measurement
     */
    private int mHeightFeet = 0;

    /**
     * Inch component of the imperial height measurement
     */
    private int mHeightInches = 0;

    /**
     * Called by the system to create the view; also does some unit conversion in preparation
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (Record.height != 0) {
            mHeightFeet = Utils.centimetersToInches(Record.height) / 12;
            mHeightInches = Utils.centimetersToInches(Record.height) % 12;
        }
        return inflater.inflate(R.layout.profile, container, false);
    }

    /**
     * Called by the system to create the view; creates callbacks for the many inputs, most of which
     * modify and write records
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getView() != null) {
            RadioGroup sexRadioGroup = getView().findViewById(R.id.profile_sex_radio);
            final EditText ageInput = getView().findViewById(R.id.profile_age_input);
            final EditText heightFeetInput = getView().findViewById(R.id.profile_height_feet_input);
            final EditText heightInchesInput = getView().findViewById(
                    R.id.profile_height_inches_input);
            final EditText heightCentimetersInput = getView().findViewById(
                    R.id.profile_height_centimeters_input);
            final EditText goalWeightInput = getView().findViewById(R.id.profile_goal_weight);
            EditText goalCaloriesInput = getView().findViewById(R.id.profile_goal_calories);
            Spinner heightSpinner = getView().findViewById(R.id.profile_height_spinner);
            Spinner goalWeightSpinner = getView().findViewById(R.id.profile_goal_weight_spinner);
            sexRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId) {
                        case R.id.profile_sex_male:
                            Record.sex = "male";
                            break;
                        case R.id.profile_sex_female:
                            Record.sex = "female";
                            break;
                    }
                    Record.write(getContext(), "sex", Record.sex);
                }
            });
            ageInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.length() == 0) {
                        Record.age = 0;
                        Record.write(getContext(), "age", 0);
                        return;
                    }
                    try {
                        Record.age = Integer.parseInt(charSequence.toString());
                    } catch (Exception e) {
                        Record.age = 0;
                    }
                    Record.write(getContext(), "age", Record.age);
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            heightFeetInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.length() == 0) {
                        if (Record.useImperialHeight) {
                            mHeightFeet = 0;
                            Record.height = Utils.inchesToCentimeters(mHeightInches);
                            Record.write(getContext(), "height", Record.height);
                        }
                        return;
                    }
                    try {
                        mHeightFeet = Integer.parseInt(charSequence.toString());
                        Record.height = Utils.inchesToCentimeters((mHeightFeet * 12) +
                                mHeightInches);
                    } catch (Exception e) {
                        mHeightFeet = 0;
                        Record.height = Utils.inchesToCentimeters(mHeightInches);
                    }
                    Record.write(getContext(), "height", Record.height);
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            heightInchesInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.length() == 0) {
                        if (Record.useImperialHeight) {
                            mHeightInches = 0;
                            Record.height = Utils.inchesToCentimeters(mHeightFeet * 12);
                            Record.write(getContext(), "height", Record.height);
                        }
                        return;
                    }
                    try {
                        mHeightInches = Integer.parseInt(charSequence.toString());
                        Record.height = Utils.inchesToCentimeters((mHeightFeet * 12) +
                                mHeightInches);
                    } catch (Exception e) {
                        mHeightInches = 0;
                        Record.height = Utils.inchesToCentimeters(mHeightFeet * 12);
                    }
                    Record.write(getContext(), "height", Record.height);
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            heightCentimetersInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.length() == 0) {
                        if (!Record.useImperialHeight) {
                            Record.height = 0;
                            Record.write(getContext(), "height", 0);
                        }
                        return;
                    }
                    try {
                        Record.height = Integer.parseInt(charSequence.toString());
                    } catch (Exception e) {
                        Record.height = 0;
                    }
                    Record.write(getContext(), "height", Record.height);
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            goalWeightInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.length() == 0) {
                        Record.goalWeight = 0;
                        Record.write(getContext(), "goal_weight", 0);
                        return;
                    }
                    try {
                        float weight = Float.parseFloat(charSequence.toString());
                        if (Record.useImperialWeight)
                            weight = Utils.poundsToKilograms(weight);
                        if (Utils.fuzzyEquals(Record.goalWeight, weight))
                            return;
                        Record.goalWeight = weight;
                    }
                    catch (Exception e) {
                        Record.goalWeight = 0;
                    }
                    Record.write(getContext(), "goal_weight", Record.goalWeight);
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            goalCaloriesInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.length() == 0) {
                        Record.goalCalories = 0;
                        Record.write(getContext(), "goal_calories", 0);
                        return;
                    }
                    int calories = Integer.parseInt(charSequence.toString());
                    if (Record.goalCalories == calories)
                        return;
                    Record.goalCalories = calories;
                    Record.write(getContext(), "goal_calories", calories);
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            heightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    // If the units selected don't match the current units on record
                    if (((i == 0) && !Record.useImperialHeight) ||
                            ((i == 1) && Record.useImperialHeight)) {
                        Record.useImperialHeight = (i == 0); // index 0 is imperial, 1 is metric
                        Record.write(getContext(), "use_imperial_height", Record.useImperialHeight);
                        // Clear previous height input views
                        if (Record.height == 0) {
                            heightFeetInput.getText().clear();
                            heightInchesInput.getText().clear();
                            heightCentimetersInput.getText().clear();
                        } else if (Record.useImperialHeight) {
                            mHeightFeet = Utils.centimetersToInches(Record.height) / 12;
                            mHeightInches = Utils.centimetersToInches(Record.height) % 12;
                            heightFeetInput.setText(String.valueOf(mHeightFeet));
                            heightInchesInput.setText(String.valueOf(mHeightInches));
                        }
                        else
                            heightCentimetersInput.setText(String.valueOf(Record.height));
                        // Show or hide the appropriate inputs
                        showOrHideViews();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
            goalWeightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (((i == 0) && !Record.useImperialWeight) ||
                            ((i == 1) && Record.useImperialWeight)) {
                        Record.useImperialWeight = (i == 0); // index 0 is imperial, 1 is metric
                        Record.write(getContext(), "use_imperial_weight", Record.useImperialWeight);
                        float goalWeight = Record.goalWeight;
                        // If the user will be entering an imperial value
                        if (Record.useImperialWeight)
                            goalWeight = Utils.kilogramsToPounds(goalWeight);
                        if (Utils.fuzzyEquals(Record.goalWeight, 0f))
                            goalWeightInput.getText().clear();
                        else
                            goalWeightInput.setText(String.format(Locale.getDefault(), "%.1f",
                                    goalWeight));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
            showOrHideViews();
        }
    }

    /**
     * Called whenever this fragment is shown; fills the inputs and labels with information from the
     * records, if it's known (previously entered by the user)
     */
    @Override
    public void refresh() {
        if (getView() != null) {
            RadioGroup sexRadioGroup = getView().findViewById(R.id.profile_sex_radio);
            EditText ageInput = getView().findViewById(R.id.profile_age_input);
            EditText heightFeetInput = getView().findViewById(R.id.profile_height_feet_input);
            EditText heightInchesInput = getView().findViewById(R.id.profile_height_inches_input);
            EditText heightCentimetersInput = getView().findViewById(
                    R.id.profile_height_centimeters_input);
            EditText goalWeightInput = getView().findViewById(R.id.profile_goal_weight);
            EditText goalCaloriesInput = getView().findViewById(R.id.profile_goal_calories);
            Spinner heightSpinner = getView().findViewById(R.id.profile_height_spinner);
            Spinner goalWeightSpinner = getView().findViewById(R.id.profile_goal_weight_spinner);
            if (Record.sex != null) {
                if (Record.sex.equals("male"))
                    sexRadioGroup.check(R.id.profile_sex_male);
                else if (Record.sex.equals("female"))
                    sexRadioGroup.check(R.id.profile_sex_female);
            }
            if (Record.age != 0)
                ageInput.setText(String.valueOf(Record.age));
            if (Record.height != 0) {
                if (Record.useImperialHeight) {
                    heightFeetInput.setText(String.valueOf(mHeightFeet));
                    heightInchesInput.setText(String.valueOf(mHeightInches));
                }
                else
                    heightCentimetersInput.setText(String.valueOf(Record.height));
            }
            else {
                heightFeetInput.getText().clear();
                heightInchesInput.getText().clear();
                heightCentimetersInput.getText().clear();
            }
            if (Record.useImperialHeight)
                heightSpinner.setSelection(0);
            else
                heightSpinner.setSelection(1);
            if (Record.goalWeight != 0f) {
                float goalWeight = Record.goalWeight;
                if (Record.useImperialWeight)
                    goalWeight = Utils.kilogramsToPounds(goalWeight);
                goalWeightInput.setText(String.format(Locale.getDefault(), "%.1f", goalWeight));
            }
            if (Record.useImperialWeight)
                goalWeightSpinner.setSelection(0);
            else
                goalWeightSpinner.setSelection(1);
            if (Record.goalCalories == 0)
                goalCaloriesInput.getText().clear();
            else
                goalCaloriesInput.setText(String.valueOf(Record.goalCalories));
        }
    }

    /**
     * Helper method for showing or hiding views which depend on the preferred units (e.g. hides the
     * foot and inch inputs while showing the centimeters input if metric height is preferred)
     */
    private void showOrHideViews() {
        if (getView() != null) {
            // Show or hide the appropriate inputs
            EditText heightFeetInput = getView().findViewById(R.id.profile_height_feet_input);
            EditText heightInchesInput = getView().findViewById(R.id.profile_height_inches_input);
            EditText heightCentimetersInput = getView().findViewById(
                    R.id.profile_height_centimeters_input);
            if (Record.useImperialHeight) {
                heightFeetInput.setVisibility(View.VISIBLE);
                heightInchesInput.setVisibility(View.VISIBLE);
                heightCentimetersInput.setVisibility(View.GONE);
            } else {
                heightFeetInput.setVisibility(View.GONE);
                heightInchesInput.setVisibility(View.GONE);
                heightCentimetersInput.setVisibility(View.VISIBLE);
            }
        }
    }
}
