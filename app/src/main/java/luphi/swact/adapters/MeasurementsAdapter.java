package luphi.swact.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import luphi.swact.R;
import luphi.swact.Record;
import luphi.swact.Utils;
import luphi.swact.models.Measurement;

/**
 * Adapter, child of ArrayAdapter, for filling ListView items displaying measurements
 */
public class MeasurementsAdapter extends ArrayAdapter<Measurement> {
    /**
     * Formatter for the time of day of each measurement entry, in the "hh:mm aa" format
     */
    private DateFormat mTimeFormat = new SimpleDateFormat("hh:mm aa", Locale.getDefault());

    /**
     * A flag indicating, if true, that night mode, or dark mode, is enabled at the system level
     */
    private boolean mIsNightModeOn;

    public MeasurementsAdapter(Context context, boolean isNightModeOn) {
        super(context, R.layout.list_item_measurements, Record.Measurements);
        mIsNightModeOn = isNightModeOn;
    }

    /**
     * Called once per list item, this fills out the various views of a measurement list item and
     * will set visibility as needed
     */
    @NonNull
    @Override
    public View getView(final int position, View listItem, @NonNull ViewGroup parent) {
        // If the list item does not exist (i.e. this is the first time viewing this item)
        if (listItem == null)
            listItem = LayoutInflater.from(getContext()).inflate(R.layout.list_item_measurements,
                    parent, false);
        if (position >= Record.Measurements.size())
            return listItem;
        Measurement measurement = Record.Measurements.get(position);
        TextView dayOfWeekText =
                listItem.findViewById(R.id.list_item_measurements_day_of_week_or_date),
                timeOfDayText = listItem.findViewById(R.id.list_item_measurements_time_of_day),
                typeText = listItem.findViewById(R.id.list_item_measurements_type),
                valueText = listItem.findViewById(R.id.list_item_measurements_value),
                noteText = listItem.findViewById(R.id.list_item_measurements_note);
        ImageButton removeButton = listItem.findViewById(R.id.list_item_measurements_remove);
        dayOfWeekText.setText(Utils.dayOfWeekOrDateString(measurement.date));
        timeOfDayText.setText(mTimeFormat.format(measurement.date));
        typeText.setText(measurement.type);
        valueText.setText(String.format(Locale.getDefault(), "%.1f", measurement.value));
        // Add a callback to the remove button to remove the entry, save, and update the list view
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Record.remove(getContext(), Record.Measurements.get(position).key());
                Record.Measurements.remove(position);
                notifyDataSetChanged();
            }
        });
        // If night mode is on, use the white icon instead
        if (mIsNightModeOn)
            removeButton.setImageResource(R.drawable.ic_delete_white_24dp);
        // Enable and set the note text, or remove the view
        if (measurement.note != null) {
            noteText.setVisibility(View.VISIBLE);
            noteText.setText(measurement.note);
        }
        else
            noteText.setVisibility(View.GONE);
        return listItem;
    }
}
