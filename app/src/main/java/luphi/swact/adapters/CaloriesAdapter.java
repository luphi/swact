package luphi.swact.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import luphi.swact.R;
import luphi.swact.Record;
import luphi.swact.Utils;
import luphi.swact.models.Calories;

/**
 * Adapter, child of ArrayAdapter, for filling ListView items displaying calories
 */
public class CaloriesAdapter extends ArrayAdapter<Calories> {
    /**
     * Formatter for the time of day of each calorie entry, in the "hh:mm aa" format
     */
    private DateFormat mTimeFormat = new SimpleDateFormat("hh:mm aa", Locale.getDefault());

    /**
     * A flag indicating, if true, that night mode, or dark mode, is enabled at the system level
     */
    private boolean mIsNightModeOn;

    public CaloriesAdapter(Context context, boolean isNightModeOn) {
        super(context, R.layout.list_item_calories, Record.Calories);
        mIsNightModeOn = isNightModeOn;
    }

    /**
     * Called once per list item, this fills out the various views of a calorie list item and will
     * set visibility as needed
     */
    @NonNull
    @Override
    public View getView(final int position, View listItem, @NonNull ViewGroup parent) {
        // If the list item does not exist (i.e. this is the first time viewing this item)
        if (listItem == null)
            listItem = LayoutInflater.from(getContext()).inflate(R.layout.list_item_calories,
                    parent, false);
        if (position >= Record.Calories.size())
            return listItem;
        Calories calories = Record.Calories.get(position);
        TextView dayOfWeekText = listItem.findViewById(R.id.list_item_calories_day_of_week_or_date),
                timeOfDayText = listItem.findViewById(R.id.list_item_calories_time_of_day),
                valueText = listItem.findViewById(R.id.list_item_calories_value),
                dailyValueText = listItem.findViewById(R.id.list_item_calories_daily_value),
                noteText = listItem.findViewById(R.id.list_item_calories_note);
        ImageButton duplicateButton = listItem.findViewById(R.id.list_item_calories_duplicate),
                removeButton = listItem.findViewById(R.id.list_item_calories_remove);
        dayOfWeekText.setText(Utils.dayOfWeekOrDateString(calories.date));
        timeOfDayText.setText(mTimeFormat.format(calories.date));
        int dailyCalories = calories.calories;
        Calendar nowCalendar = Calendar.getInstance(), pastCalendar = Calendar.getInstance();
        nowCalendar.setTime(calories.date);
        for (int i = position + 1; i < Record.Calories.size(); i++) {
            Calories pastCalories = Record.Calories.get(i);
            pastCalendar.setTime(pastCalories.date);
            if (!Utils.isSameDay(nowCalendar, pastCalendar))
                break;
            dailyCalories += pastCalories.calories;
        }
        // If the daily caloric consumption is more than 100 kilocalories above the goal
        if (dailyCalories > (Record.goalCalories + 100)) {
            valueText.setTextColor(getContext().getResources().getColor(R.color.color_primary));
            dailyValueText.setTextColor(getContext().getResources().getColor(
                    R.color.color_primary));
        }
        // If the daily caloric consumption is within 100 kilocalories of the goal
        else if (Math.abs(Record.goalCalories - dailyCalories) <= 100f) {
            valueText.setTextColor(getContext().getResources().getColor(R.color.color_accent));
            dailyValueText.setTextColor(getContext().getResources().getColor(R.color.color_accent));
        }
        // If the value is below the goal
        else {
            // Set the text colors to that of the time of day TextView; this is the simplest way to
            // set/revert the text color to the theme's default
            valueText.setTextColor(timeOfDayText.getTextColors());
            dailyValueText.setTextColor(timeOfDayText.getTextColors());
        }
        valueText.setText(String.valueOf(calories.calories));
        dailyValueText.setText(String.valueOf(dailyCalories));
        // Add a callback to the duplicate button to make a copy of the entry at the current date
        // and time, save, and update the list view
        duplicateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calories calories = Record.Calories.get(position),
                        copyCalories = new Calories(new Date(), calories.calories, calories.note);
                Record.Calories.add(0, copyCalories);
                Record.write(getContext(), copyCalories.key(), copyCalories.value());
                notifyDataSetChanged();
            }
        });
        // Add a callback to the remove button to remove the entry, save, and update the list view
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Record.remove(getContext(), Record.Calories.get(position).key());
                Record.Calories.remove(position);
                notifyDataSetChanged();
            }
        });
        // If night mode is on, use the white icons instead
        if (mIsNightModeOn) {
            duplicateButton.setImageResource(R.drawable.ic_content_copy_white_24dp);
            removeButton.setImageResource(R.drawable.ic_delete_white_24dp);
        }
        // Enable and set the note text, or remove the view
        if (calories.note != null) {
            noteText.setVisibility(View.VISIBLE);
            noteText.setText(calories.note);
        }
        else
            noteText.setVisibility(View.GONE);
        return listItem;
    }
}
