package luphi.swact.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import luphi.swact.R;
import luphi.swact.Record;
import luphi.swact.Utils;
import luphi.swact.models.Weight;

/**
 * Adapter, child of ArrayAdapter, for filling ListView items displaying weights
 */
public class WeightAdapter extends ArrayAdapter<Weight> {
    /**
     * Formatter for the time of day of each weight entry, in the "hh:mm aa" format
     */
    private DateFormat mTimeFormat = new SimpleDateFormat("hh:mm aa", Locale.getDefault());

    /**
     * A flag indicating, if true, that night mode, or dark mode, is enabled at the system level
     */
    private boolean mIsNightModeOn;

    public WeightAdapter(Context context, boolean isNightModeOn) {
        super(context, R.layout.list_item_weight, Record.Weights);
        mIsNightModeOn = isNightModeOn;
    }

    /**
     * Called once per list item, this fills out the various views of a weight list item and will
     * set visibility as needed
     */
    @NonNull
    @Override
    public View getView(final int position, View listItem, @NonNull ViewGroup parent) {
        // If the list item does not exist (i.e. this is the first time viewing this item)
        if (listItem == null)
            listItem = LayoutInflater.from(getContext()).inflate(R.layout.list_item_weight, parent,
                    false);
        if (position >= Record.Weights.size())
            return listItem;
        Weight weight = Record.Weights.get(position);
        TextView dayOfWeekText = listItem.findViewById(R.id.list_item_weight_day_of_week_or_date),
                timeOfDayText = listItem.findViewById(R.id.list_item_weight_time_of_day),
                deltaText = listItem.findViewById(R.id.list_item_weight_delta),
                valueText = listItem.findViewById(R.id.list_item_weight_value),
                unitsText = listItem.findViewById(R.id.list_item_weight_units),
                noteText = listItem.findViewById(R.id.list_item_weight_note);
        ImageButton removeButton = listItem.findViewById(R.id.list_item_weight_remove);
        dayOfWeekText.setText(Utils.dayOfWeekOrDateString(weight.date));
        timeOfDayText.setText(mTimeFormat.format(weight.date));
        String deltaFormattedText = "";
        if (position < (Record.Weights.size() - 1)) {
            float delta = weight.weight - Record.Weights.get(position + 1).weight;
            if (Record.useImperialWeight)
                delta = Utils.kilogramsToPounds(delta);
            if (Math.abs(delta) < 0.1f)
                deltaFormattedText = "0";
            else if (delta > 0f)
                deltaFormattedText = "+" + String.format(Locale.getDefault(), "%.1f", delta);
            else
                deltaFormattedText = String.format(Locale.getDefault(), "%.1f", delta);
            float closeEnough = 0.5f; // 0.5 kilograms is close enough to the goal
            if (Record.useImperialWeight)
                closeEnough = Utils.kilogramsToPounds(closeEnough); // ~1.1 pounds
            float distanceFromGoal = Record.goalWeight - weight.weight;
            if (Record.useImperialWeight)
                distanceFromGoal = Utils.kilogramsToPounds(distanceFromGoal);
            // If there was a non-negligent change
            if (Math.abs(delta) >= 0.1f) {
                // If the change is towards the goal weight or the weight is close to the goal
                if (((distanceFromGoal * delta) >= 0f) ||
                        (Math.abs(distanceFromGoal) <= closeEnough))
                    deltaText.setTextColor(getContext().getResources().getColor(
                            R.color.color_accent));
                // If the change is away from the goal weight
                else
                    deltaText.setTextColor(getContext().getResources().getColor(
                            R.color.color_primary));
            }
            // If the change isn't noteworthy or non-existent
            else
                // Set the text colors to that of the time of day TextView; this is the simplest
                // way to set/revert the text color to the theme's default
                deltaText.setTextColor(timeOfDayText.getTextColors());
        }
        deltaText.setText(deltaFormattedText);
        float weightValue = weight.weight;
        if (Record.useImperialWeight)
            weightValue = Utils.kilogramsToPounds(weightValue);
        valueText.setText(String.format(Locale.getDefault(), "%.1f", weightValue));
        if (Record.useImperialWeight)
            unitsText.setText(getContext().getResources().getString(
                    R.string.imperial_weight_units));
        else
            unitsText.setText(getContext().getResources().getString(R.string.metric_weight_units));
        // Add a callback to the remove button to remove the entry, save, and update the list view
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Record.remove(getContext(), Record.Weights.get(position).key());
                Record.Weights.remove(position);
                notifyDataSetChanged();
            }
        });
        // If night mode is on, use the white icon instead
        if (mIsNightModeOn)
            removeButton.setImageResource(R.drawable.ic_delete_white_24dp);
        // Enable and set the note text, or remove the view
        if (weight.note != null) {
            noteText.setVisibility(View.VISIBLE);
            noteText.setText(weight.note);
        }
        else
            noteText.setVisibility(View.GONE);
        return listItem;
    }
}
