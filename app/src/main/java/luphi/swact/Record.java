package luphi.swact;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.format.DateUtils;

import androidx.preference.PreferenceManager;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import luphi.swact.models.Calories;
import luphi.swact.models.Measurement;
import luphi.swact.models.Weight;

/**
 * A collection of static objects and methods that represent user's entries (of weight, calories,
 * and measurements), preferences, and routines for both reading and writing that information.  In
 * addition, it contains helper methods (e.g. summing the calories entered so far today).
 */
public class Record {
    /**
     * A flag indicating, if true, that the records have already been read
     */
    private static boolean mIsRead = false;

    /**
     * A list of all weight entries entered by the user, past or present
     */
    public static List<Weight> Weights = new LinkedList<>();

    /**
     * A list of all calorie entries entered by the user, past or present
     */
    public static List<Calories> Calories = new LinkedList<>();

    /**
     * A list of all measurement entries entered by the user, past or present
     */
    public static List<Measurement> Measurements = new LinkedList<>();

    /**
     * A list of all measurement types (most likely body parts) created by the user
     */
    public static List<String> MeasurementsTypes = new LinkedList<>();

    /**
     * The user's sex; null if not yet known, or either "male" or "female"
     */
    public static String sex = null;

    /**
     * The user's age, in years; zero if not yet known
     */
    public static int age = 0;

    /**
     * The user's height, in centimeters; zero if not yet known
     */
    public static int height = 0;

    /**
     * The user's weight, in kilograms; zero if not yet known
     */
    public static float goalWeight = 0f;

    /**
     * The user's daily caloric consumption goal, in kilocalories; zero if not yet known
     */
    public static int goalCalories = 0;

    /**
     * A flag indicating the user's preferred weight units, imperial if true; defaults to metric
     */
    public static boolean useImperialWeight = false;

    /**
     * A flag indicating the user's preferred height units, imperial if true; defaults to metric
     */
    public static boolean useImperialHeight = false;

    /**
     * Index in the activity level array, used by and selected in the TDEE calculator fragment,
     * indicating the user's estimated level of activity/exercise; defaults to zero (sedentary)
     */
    public static int activityLevel = 0;

    /**
     * A comparator for sorting weight entries by date
     */
    public static class WeightDateComparator implements Comparator<Weight> {
        public int compare(Weight one, Weight two) {
            return two.date.compareTo(one.date);
        }
    }

    /**
     * A comparator for sorting calorie entries by date
     */
    public static class CaloriesDateComparator implements Comparator<Calories> {
        public int compare(Calories one, Calories two) {
            return two.date.compareTo(one.date);
        }
    }

    /**
     * A comparator for sorting measurement entries by date
     */
    public static class MeasurementsDateComparator implements Comparator<Measurement> {
        public int compare(Measurement one, Measurement two) {
            return two.date.compareTo(one.date);
        }
    }

    /**
     * Read all known information through the SharedPreferences object into the above members
     *
     * @param context The application Context
     */
    static void read(Context context) {
        if (mIsRead)
            return;
        SharedPreferences sharedPreferences;
        try {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        } catch (Exception e) {
            return;
        }
        if (sharedPreferences != null) {
            Map<String, ?> keys = sharedPreferences.getAll();
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                // Weight keys are in the form "weight 1274859304" for which the first integer is
                // the milliseconds since the Unix epoch
                if (entry.getKey().startsWith("weight ")) {
                    Weights.add(new Weight(entry.getKey(), entry.getValue().toString()));
                }
                // Calorie keys are in the form "calories 1274859304" for which the first integer is
                // the milliseconds since the Unix epoch
                else if (entry.getKey().startsWith("calories ")) {
                    Calories.add(new Calories(entry.getKey(), entry.getValue().toString()));
                }
                // Measurement keys are in the form "measurements 1274859304" for which the first
                // integer is the milliseconds since the Unix epoch
                else if (entry.getKey().startsWith("measurements ")) {
                    Measurements.add(new Measurement(entry.getKey(), entry.getValue().toString()));
                }
                // Measurement type keys are simply "measurements_type"
                else if (entry.getKey().startsWith("measurements_type")) {
                    MeasurementsTypes.add(entry.getValue().toString());
                }
            }
            // Sort the records by date as they aren't necessarily read in order
            Collections.sort(Record.Weights, new WeightDateComparator());
            Collections.sort(Record.Calories, new CaloriesDateComparator());
            Collections.sort(Record.Measurements, new MeasurementsDateComparator());
            sex = sharedPreferences.getString("sex", "");
            age = sharedPreferences.getInt("age", 0);
            height = sharedPreferences.getInt("height", 0);
            goalWeight = sharedPreferences.getFloat("goal_weight", 0f);
            goalCalories = sharedPreferences.getInt("goal_calories", 0);
            useImperialHeight = sharedPreferences.getBoolean("use_imperial_height", false);
            useImperialWeight = sharedPreferences.getBoolean("use_imperial_weight", false);
            activityLevel = sharedPreferences.getInt("activity_level", 0);
            // TODO remove this
            /* Random random = new Random();
            for (int i = 0; i < 30; i += 3) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, -i);
                Weight weight = new Weight("weight " + calendar.getTimeInMillis(),
                        "" + Utils.poundsToKilograms(160 + i + (random.nextInt(4) - 2)));
                Weights.add(weight);
            }
            for (int i = 2; i < 60; i += (1 + random.nextInt(2))) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, -i);
                Calories calories = new Calories("calories " + calendar.getTimeInMillis(),
                        "" + (random.nextInt(200) + 1700));
                Calories.add(calories);
            }
            MeasurementsTypes.add("Biceps");
            MeasurementsTypes.add("Thighs");
            for (int i = 0; i < 60; i += (1 + random.nextInt(2))) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, -i);
                Measurement measurements = new Measurement(
                        "measurements " + calendar.getTimeInMillis(),
                        "Biceps " + (14 + random.nextInt(6)));
                if (random.nextBoolean())
                    Measurement.add(measurements);
                if (random.nextBoolean()) {
                    measurements.type = "Thighs";
                    Measurement.add(measurements);
                }
            } */
        }
        mIsRead = true;
    }

    /**
     * Write all known information from the above members through the SharedPreferences object
     *
     * @param context The application Context
     */
    static void write(Context context) {
        SharedPreferences.Editor sharedPreferencesEditor =
                PreferenceManager.getDefaultSharedPreferences(context).edit();
        for (Weight weight : Weights)
            sharedPreferencesEditor.putString(weight.key(), weight.value());
        for (Calories calories : Calories)
            sharedPreferencesEditor.putString(calories.key(), calories.value());
        for (Measurement measurement : Measurements)
            sharedPreferencesEditor.putString(measurement.key(), measurement.value());
        for (String measurementsType : MeasurementsTypes)
            sharedPreferencesEditor.putString("measurements_type", measurementsType);
        sharedPreferencesEditor.putString("sex", sex);
        sharedPreferencesEditor.putInt("age", age);
        sharedPreferencesEditor.putInt("height", height);
        sharedPreferencesEditor.putFloat("goal_weight", goalWeight);
        sharedPreferencesEditor.putInt("goal_calories", goalCalories);
        sharedPreferencesEditor.putBoolean("use_imperial_height", useImperialHeight);
        sharedPreferencesEditor.putBoolean("use_imperial_weight", useImperialWeight);
        sharedPreferencesEditor.putInt("activity_level", activityLevel);
        sharedPreferencesEditor.apply();
    }

    /**
     * Write a single key-value pair for which the value is a string
     *
     * @param context The application Context
     * @param key The key to be used by SharedPreferences
     * @param value The value as a string
     */
    public static void write(Context context, String key, String value) {
        SharedPreferences.Editor sharedPreferencesEditor =
                PreferenceManager.getDefaultSharedPreferences(context).edit();
        sharedPreferencesEditor.putString(key, value);
        sharedPreferencesEditor.apply();
    }

    /**
     * Write a single key-value pair for which the value is an integer
     *
     * @param context The application Context
     * @param key The key to be used by SharedPreferences
     * @param value The value as an integer
     */
    public static void write(Context context, String key, int value) {
        SharedPreferences.Editor sharedPreferencesEditor =
                PreferenceManager.getDefaultSharedPreferences(context).edit();
        sharedPreferencesEditor.putInt(key, value);
        sharedPreferencesEditor.apply();
    }

    /**
     * Write a single key-value pair for which the value is a float
     *
     * @param context The application Context
     * @param key The key to be used by SharedPreferences
     * @param value The value as a float
     */
    public static void write(Context context, String key, float value) {
        SharedPreferences.Editor sharedPreferencesEditor =
                PreferenceManager.getDefaultSharedPreferences(context).edit();
        sharedPreferencesEditor.putFloat(key, value);
        sharedPreferencesEditor.apply();
    }

    /**
     * Write a single key-value pair for which the value is a boolean
     *
     * @param context The application Context
     * @param key The key to be used by SharedPreferences
     * @param value The value as a boolean
     */
    public static void write(Context context, String key, boolean value) {
        SharedPreferences.Editor sharedPreferencesEditor =
                PreferenceManager.getDefaultSharedPreferences(context).edit();
        sharedPreferencesEditor.putBoolean(key, value);
        sharedPreferencesEditor.apply();
    }

    /**
     * Get today's weight, or zero if no weight has yet been recorded
     *
     * @return Today's weight, or zero if no weight has been entered today
     */
    public static float todaysWeight() {
        if (Weights.isEmpty() || !DateUtils.isToday(Weights.get(0).date.getTime()))
            return 0f;
        return Weights.get(0).weight;
    }

    /**
     * Get the most recently-recorded weight, or zero if no records exist
     *
     * @return The most recent weight entered, or zero if none exist
     */
    public static float mostRecentWeight() {
        if (Weights.isEmpty())
            return 0f;
        return Weights.get(0).weight;
    }

    /**
     * Sum the calories recorded so far today, or return zero if none exist
     *
     * @return Total calories recorded today, or zero if none were entered today
     */
    public static int todaysCalories() {
        if (Calories.isEmpty())
            return 0;
        int dailyCalories = 0;
        for (Calories calories : Calories) {
            if (!DateUtils.isToday(calories.date.getTime()))
                break;
            dailyCalories += calories.calories;
        }
        return dailyCalories;
    }

    /**
     * Remove all records of this app from disk; this does not clear the values held in memory
     *
     * @param context The application Context
     */
    static void clear(Context context) {
        SharedPreferences.Editor sharedPreferencesEditor =
                PreferenceManager.getDefaultSharedPreferences(context).edit();
        sharedPreferencesEditor.clear();
        sharedPreferencesEditor.apply();
    }

    /**
     * Remove the recording with the given key from disk; this does not remove the record held in
     * memory
     *
     * @param context The application Context
     * @param key The key of the entry to remove
     */
    public static void remove(Context context, String key) {
        SharedPreferences.Editor sharedPreferencesEditor =
                PreferenceManager.getDefaultSharedPreferences(context).edit();
        sharedPreferencesEditor.remove(key);
        sharedPreferencesEditor.apply();
    }
}
